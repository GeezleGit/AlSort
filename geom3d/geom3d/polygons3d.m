function polygons3d(varargin)
%POLYGONS3D Description of functions operating on 3D polygons
%
%   A 3D polygon is simply a set of 3D points which are supposed to be
%   located in the same plane.
%
%   See also:
%   polygon3dNormalAngle, polygonCentroid3d
%   clipPolygon3dHP, clipConvexPolygon3dHP
%   drawPolyline3d, fillPolygon3d
%
%
% ------
% Author: David Legland
% e-mail: david.legland@nantes.inra.fr
% Created: 2008-10-13,    using Matlab 7.4.0.287 (R2007a)
% Copyright 2008 INRA - BIA PV Nantes - MIAJ Jouy-en-Josas.
