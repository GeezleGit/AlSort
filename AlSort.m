classdef AlSort
    
    properties
        
        WorkDir = '';
        FileNames = {};
        FileTimes = [];
        
        Data = [];% nse file
        Events = [];% nev file
        DataAnalog = [];
        
        TimeRange = [];% possible range of values 
        DigitalRange = [];% possible range of values
        
        FeatureLim = [];% field of view, i.e. axes limits 
        FeatureWin = [];% region of interest, analysis window
        TimeLim = [];% field of view, i.e. axes limits 
        TimeWin = [];% region of interest, analysis window
        
        FeatureValue = [];%[nSpikes x nFeat]
        FeatureName = {};
        FeatureList = {};
        
        WaveformFeatureIndex = [];% index for Featurename to construct waveform
        
        currentCluster = [];
        ClusterNr = [];
        ClusterBounds = cell(0,32);%[2 x nFeatures x nClust]
        ClusterColor = [];
        
        SortBuffer = false(0,0,0);% [n,ClusterNr,nBuffer] logical index of timestamps 
        nBuffer = 5;
        
        nWaveform = 5000;
        TimeWinProp = {};
        ClusterProp = {};
        
    end
    
    methods
        function SS = AlSort(SS1,varargin)
            % AlSort constructor
            %
            % AlSort = AlSort(AlSort,Struct)
            % converts a structure to AlSort-class
            %
            % AlSort = AlSort(AlSort,'property1',[value1], ...)
            % creates object and sets values of given properties
            %
            if nargin>1
                f1 = fieldnames(SS1);
                SS = SS1;
                if length(varargin)==1 && isstruct(varargin{1})
                    % copy fields of structure
                    DF2 = varargin{1};
                    f2 = fieldnames(DF2);
                    for i=1:length(f2)
                        if ~any(strcmp(f2{i},f1));continue;end
                        SS.(f2{i}) = DF2.(f2{i});
                    end
                elseif  rem(length(varargin),2)==0
                    % set fields
                    f2 = varargin(1:2:end);
                    v2 = varargin(2:2:end);
                    for j = 1:numel(SS1)
                        for i=1:length(f2)
                            if ~any(strcmp(f2{i},f1));continue;end
                            SS(j).(f2{i}) = v2{i};
                        end
                    end
                end
            else
                
            end
        end
                

        function A = AlSort_initiate(A)
            % Initiates the AlSort object, set object properties to current data
            
            A.TimeRange = NLX_getTimeRangeNSE(A.Data);
            A.TimeLim = A.TimeRange;
            A.TimeWin = A.TimeRange;
            
            [A.DigitalRange,A.FeatureWin] = NLX_getADRangeNSE(A.Data);
            A.FeatureLim = A.FeatureWin;
            
            A.ClusterNr = unique(A.Data.ClusterNr);
            nCluster = length(A.ClusterNr);
            A.currentCluster = 1;
            A.SortBuffer = false(length(A.Data.TimeStamps),nCluster,A.nBuffer);
            for iC = 1:nCluster
                A.SortBuffer(:,iC,1) = NLX_findSpikes(A.Data,'CLUSTER',A.ClusterNr(iC));
            end
            
            A.TimeWinProp = {'linewidth',2,'color',[1 1 1]};
            A.ClusterProp = {'markersize',1,'marker','o','markeredgecolor','none','linestyle','none'};
            A.ClusterColor(1,:,:) = cat(3,rgb('dark gray'),rgb('light gray'));
            A.ClusterColor(2,:,:) = cat(3,rgb('dark red'),rgb('red'));
            A.ClusterColor(3,:,:) = cat(3,rgb('dark yellow'),rgb('yellow'));
            A.ClusterColor(4,:,:) = cat(3,rgb('dark green'),rgb('light green'));
            A.ClusterColor(5,:,:) = cat(3,rgb('slightly dark blue'),rgb('light blue'));
            A.ClusterColor(6,:,:) = cat(3,rgb('dark orange'),rgb('light orange'));
            A.ClusterColor(7,:,:) = cat(3,rgb('dark olive'),rgb('light olive'));
            A.ClusterColor(8,:,:) = cat(3,rgb('dark turquoise'),rgb('turquoise'));
            A.ClusterColor(9,:,:) = cat(3,rgb('dark maroon'),rgb('light maroon'));
            A.ClusterColor(10,:,:) = cat(3,rgb('dark u6'),rgb('light u6'));
            A.ClusterColor(11,:,:) = cat(3,rgb('dark u4'),rgb('light u4'));
            A.ClusterColor(12,:,:) = cat(3,rgb('dark u6'),rgb('light u6'));
            A.ClusterColor(13,:,:) = cat(3,rgb('dark u3'),rgb('light u3'));
            A.ClusterColor(14,:,:) = cat(3,rgb('dark u5'),rgb('light u5'));
            A.ClusterColor(15,:,:) = cat(3,rgb('dark u2'),rgb('light u2'));
            
            A.FeatureList = { ...
                '#1';'#2';'#3';'#4';'#5';'#6';'#7';'#8';'#9';'#10'; ...
                '#11';'#12';'#13';'#14';'#15';'#16';'#17';'#18';'#19';'#20'; ...
                '#21';'#22';'#23';'#24';'#25';'#26';'#27';'#28';'#29';'#30'; ...
                '#31';'#32'; ...
                'Peak';'Valley';'Height';'Energy'; ...
                'PC1';'PC2';'PC3'};
            A.WaveformFeatureIndex = [1:32];

            
        end
        
        function A = AlSort_setCurrentCluster(A,ClusterNr)
            if ischar(ClusterNr)
                ClusterNr = str2double(ClusterNr);
            end
            A.currentCluster = find(A.ClusterNr==ClusterNr);
            if isempty(A.currentCluster)
                error('Can'' find cluster #%1.0f !',ClusterNr);
            end
        end
        
		
		function A = AlSort_computePrincipalComponents(A,ClusterNrs,UseTimeWindow,WaveFormIndex)
			
            % which clusternr
			if nargin<2 || isempty(ClusterNrs)
				TSIndex = true(size(A.Data.TimeStamps,1),1);
			else
				TSIndex = any(A.SortBuffer(:,ismember(A.ClusterNr,ClusterNrs),1),2);
            end
            
            % time window
			if nargin>2 && UseTimeWindow
				TSIndex = TSIndex & A.Data.TimeStamps>=A.TimeWin(1) & A.Data.TimeStamps<=A.TimeWin(2);
            end
			
            % which waveform samples to use
            if nargin<4 || isempty(WaveFormIndex)
                WaveFormIndex = 1:32;
            end
				
			A.Data.PrinComp = zeros(size(A.Data.TimeStamps,1),3).*NaN;
			A.Data.PrinComp(TSIndex,:) = NLX_WaveformFeature(A.Data,'PC',TSIndex,WaveFormIndex,'POSSYSRANGE',true);
		end
        
        function [Data,DataLim] = AlSort_getFeature(A,FName,TSindex)
            if nargin<3
                TSindex = [];
            end
            if any(strcmp(FName,{'Height' 'Energy'}))
                [Data,DataLim] = NLX_WaveformFeature(A.Data,FName,TSindex,[],'POSSYSRANGE',true);
            elseif any(strcmp(FName,{'PC1' 'PC2' 'PC3'}))
				if isfield(A.Data,'PrinComp')
                    if isempty(TSindex)
                        Data = A.Data.PrinComp(:,strcmp(FName,{'PC1' 'PC2' 'PC3'}));
                    elseif ~isempty(TSindex)
                        Data = A.Data.PrinComp(TSindex,strcmp(FName,{'PC1' 'PC2' 'PC3'}));
                    end
                else
                    errordlg('Principle components haven''t been computed yet!','get waveform features');
				end
				DataLim = [min(Data(:)) max(Data(:))];
            else
                [Data,DataLim] = NLX_WaveformFeature(A.Data,FName,TSindex);
            end
        end
        
        function Index = AlSort_inFeaturePolygon(A,FeatureName,PolygonValues,SpaceRotation)
            %
            % "PolygonValues" are waveform boundaries
            % FeatureName ..... [1 x n] sample nr e.g. [1:32]
            % PolygonValues ... [2 x n] upper and lower boundaries for each
            %                   sample
            % 
            % "PolygonValues" from cluster cutting in axis
            % FeatureName ..... 
            % PolygonValues ... 'currentpoint' axes-property
            % SpaceRotation ... defines the rotation in space spanned by FeatureName
            %                   [3by3] CameraPosition,CameraTarget,CameraUpVector
            
            if isnumeric(FeatureName) && length(FeatureName)==size(PolygonValues,2)
                Index = NLX_findSpikes(A.Data,'WAVEFORM',[FeatureName(:) PolygonValues']);
                return;
            end
            
            nFeat = length(FeatureName);
            nSpks = length(A.Data.TimeStamps);
            if nargin<4
                SpaceRotation = [];
            end
            
            % collect feature values
            FeatValues = zeros(nSpks,nFeat).*NaN;
            for iF = 1:nFeat
                FeatValues(:,iF) = AlSort_getFeature(A,FeatureName{iF});
            end
            
            fv = FeatValues;
            pv = permute(PolygonValues(1,:,:),[3,2,1]);
            
            % rotate 3D space to use 2D inpolygon function
            if ~isempty(SpaceRotation)
                LineOfView = SpaceRotation(:,1)-SpaceRotation(:,2);
                PlaneOfViewVert = createPlane(SpaceRotation');
                BaseX = planeNormal(PlaneOfViewVert);
                PlaneOfViewHor = createPlane([SpaceRotation(:,[1 2])';BaseX]);
                BaseY = planeNormal(PlaneOfViewHor);
                NewBasis = [ ...
                    LineOfView' ... z
                    BaseX ... x
                    BaseY ... y
                    ];
                TRANSFO = createBasisTransform3d(NewBasis);
                
                fv = transformPoint3d(FeatValues,TRANSFO);
                pv = transformPoint3d(permute(PolygonValues(1,:,:),[3,2,1]),TRANSFO);
            end
            
            %% do 2D-inpolygon
            Index = inpolygon(fv(:,1),fv(:,2),pv(:,1),pv(:,2));
        end
        
                       
        function A = AlSort_addCluster(A)
            A = AlSort_SortBuffer(A,'CLEARCLUST');
            A = AlSort_SortBuffer(A,'NEW');
            A = AlSort_SortBuffer(A,'ADDCLUST');
            A = AlSort_SortBuffer(A,'SORTCLUST');
        end
        
        function A = AlSort_deleteCluster(A,ClusterNr)
            ClusterNr(ClusterNr==0) = [];
            if isempty(ClusterNr);return;end
            ButtonName = questdlg(sprintf('Do you want to delete clusters ?'),'deleting cluster ...');
            if ~strcmp(ButtonName,'Yes');return;end
            
            A = AlSort_SortBuffer(A,'NEW');% create new buffer first
            for i=1:length(ClusterNr)
                A.SortBuffer(A.SortBuffer(:,A.ClusterNr==ClusterNr(i)),A.ClusterNr==0,1) = true;% transfer to zero cluster
                A.SortBuffer(:,A.ClusterNr==ClusterNr(i),1)  = false;% delete cluster
            end
            A.currentCluster = 1;
            A = AlSort_SortBuffer(A,'CLEARCLUST');
        end
        
        function A = AlSort_copyCluster(A)
            if A.ClusterNr(A.currentCluster) == 0
                warning('You can''t copy cluster #0!');
                return;
            end
            A = AlSort_SortBuffer(A,'NEW');
            iSourceClust = A.currentCluster;
            A = AlSort_SortBuffer(A,'ADDCLUST');
            iTargetClust = A.currentCluster;
            A.SortBuffer(:,iTargetClust,1) = A.SortBuffer(:,iSourceClust,1);
            A = AlSort_SortBuffer(A,'SORTCLUST');
        end
        
        function A = AlSort_mergeCluster(A,SourceClust,TargetClust)
            % this uses the cluster tickboxes to define clusters to merge
            
            if TargetClust~=0 && any(SourceClust==0)
                warning('You can''t merge cluster #0 into another cluster!');
                return;
            elseif TargetClust==0 && any(SourceClust~=0)
                % That's fine;
            elseif TargetClust==0 && any(SourceClust==0)
                SourceClust(SourceClust==0) = [];
                if isempty(SourceClust)
                    return;
                end
            end
            
            iSource = ismember(A.ClusterNr,SourceClust);
            iTarget = A.currentCluster;
            iClearance = iSource;
            iClearance(iTarget) = false;

            A = AlSort_SortBuffer(A,'NEW');
            A.SortBuffer(:,iTarget,1) = A.SortBuffer(:,iTarget,1) | any(A.SortBuffer(:,iSource,1),2);
            A.SortBuffer(:,iClearance,1)  = false;% delete cluster
            
            A = AlSort_SortBuffer(A,'SORTCLUST');
            A = AlSort_SortBuffer(A,'CLEARCLUST');            
            
        end
       
        function [isDup,DupComb] = AlSort_duplicateClusters(A)
            [nTs,nC,nUndos] = size(A.SortBuffer(:,:,1));
            isDup = sum(A.SortBuffer(:,:,1),2)>1;
            DupComb = unique(A.SortBuffer(isDup,:,1),'rows');
            currComb = false(size(A.SortBuffer(:,:,1)));
            for i=1:size(DupComb,1)
                currComb = repmat(DupComb(i,:),nTs,1);
                isDup(:,i) = all(currComb==A.SortBuffer(:,:,1),2);
            end
        end
        
        function [ok,A] = AlSort_CheckClustering(A)
            A.SortBuffer(sum(A.SortBuffer(:,:,1),2)==0,A.ClusterNr==0,1) = true; % unassigned to zero
            if any(sum(A.SortBuffer(:,:,1),2)>1)
                [isDup,DupComb] = AlSort_duplicateClusters(A);
                fprintf(1,'Found spikes being members of more than 1 cluster:\n');
                for i=1:size(DupComb,1)
                    disp(find(DupComb(i,:))-1);
                end
                
                % remove duplicates that consist of zero + another
                for i=1:size(DupComb,1)
                    if DupComb(i,1) && sum(DupComb(i,2:end))==1
                        % set zero cluster to false for this pair
                        A.SortBuffer(all(A.SortBuffer(:,DupComb(i,:),1),2),1,1) = false;
                    end
                end
            end
            
            % final check
            if ~all(sum(A.SortBuffer(:,:,1),2)==1)
                ok = false;
            else
                ok = true;
            end

        end
        
        
        function A = AlSort_SortBuffer(A,option,varargin)
            switch option
                case 'BACK'
                    A.SortBuffer = circshift(A.SortBuffer,[0 0 -1]);
                case 'FORWARD'
                    A.SortBuffer = circshift(A.SortBuffer,[0 0 1]);
                case 'NEW'
                    A.SortBuffer = circshift(A.SortBuffer,[0 0 1]);
                    A.SortBuffer(:,:,1) = A.SortBuffer(:,:,2);
                case 'CLEARCLUST'
                    EmptyClust = ~any(any(A.SortBuffer,1),3);
                    A.SortBuffer(:,EmptyClust,:) = [];
                    A.ClusterNr(EmptyClust) = [];
                    if EmptyClust(A.currentCluster) || A.currentCluster>length(A.ClusterNr)
                        A.currentCluster = 1;
                    end
                case 'ADDCLUST'
                    A = AlSort_SortBuffer(A,'SORTCLUST');
                    emptyClust = ~any(A.SortBuffer(:,:,1),1);
                    if ~any(emptyClust)
                        iC = length(A.ClusterNr)+1;
                        xC = [0:max(A.ClusterNr+1)];
                        nC = min(xC(~ismember(xC,A.ClusterNr)));
                    else
                        iC = find(emptyClust,1,'first');
                        nC = A.ClusterNr(iC);
                    end
                    A.ClusterNr(iC) = nC;
                    A.SortBuffer(:,iC,:) = false;
                    A.currentCluster = iC;
                case 'SORTCLUST'
                    [A.ClusterNr,SortIdx] = sort(A.ClusterNr);
                    A.SortBuffer = A.SortBuffer(:,SortIdx,:);
                    A.currentCluster = SortIdx(A.currentCluster);
                otherwise
                    error('AlSort_SortBuffer: Don''t know option "%s"',option);
            end
        end
        
        

        

    end
end


