######
AlSort
######

GUI for manual spike sorting of neuralynx spikes in matlab

Installation
============

* Copy AlSort toolbox
* Copy the folder “…\ThieleGroup\LAB STUFF\AlSort” to your local machine.
* Copy modified ginput.m
* To work in 3D plots you have to modify the ginput function (selection of data within Matlab axes using the mouse). Just replace
  the existing ginput.m with the modified function ginput.mxx.

	1. Rename “MATLAB directory …\toolbox\matlab\uitools\ginput.m” to something like “MATLAB directory …\toolbox\matlab\uitools\ginput_old.m”  
	2. Copy “... \AlSort\misc\ginput.mxx” to “MATLAB directory …\toolbox\matlab\uitools\”
	3. Rename the copied “ginput.mxx” to “ginput.m”

Start a sorting session
=======================

1. Change the matlab current folder to the “AlSort”- folder that you copied to your local machine.
2. At the command prompt type `AlSort_start`, which will add directories to your matlab path.
3. For convenience, change the current folder of Matlab to a directory near the directory where you keep your Neuralynx data files.

AlSort - Bug/to do list
=======================
* better control of axes limits of Feature vs Feature plot, replace
  sliders for feature limits with textedits -> done
* polygon selection: use 2 mouse buttons to select to add or to exclude selected clusters
* extend a selected polygon in a current time window to all waveforms
* change coloring when using Sort-Mode
* polygon selection: set clipping off when drawing selection
