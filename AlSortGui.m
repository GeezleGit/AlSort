function varargout = AlSortGui(CallString,varargin)

output = feval(sprintf('call_%s',CallString),varargin{:});
varargout = output;

end




function output = call_start(varargin)
output = {[]};
FunPath = which('AlSortGui');
AlSortDir = fileparts(FunPath);
P = {AlSortDir};
P = cat(1,P,{[fullfile(AlSortDir,'geom3d','geom3d')]});
P = cat(1,P,{[fullfile(AlSortDir,'geom3d','meshes3d')]});
% P = cat(1,P,{[fullfile(AlSortDir,'Neuralynx-Tools')]});
P = cat(1,P,{[fullfile(AlSortDir,'misc')]});
fprintf(1,'Trying to add the following to matlab path:\n');
fprintf(1,'%s\n',P{:});
addpath(P{:});

h = findobj('tag','AlSortGui_SortControl');
if isempty(h)
    h = createSortControl;
end
AlSortGui('store',AlSort);
%fprintf(1,'TIP: Change your current folder near to your data directory. \n')
end

function output = call_open(varargin)
output = {[]};
fPath = varargin{1};
ClustNr = varargin{2};
AlSortGui('start');
A = AlSort;
A.Data = NLX_LoadNSE(fPath,'full',1,[]);
A = AlSort_initiate(A);
A = AlSortGui('setSortControl',A,'default');
if any(A.ClusterNr==ClustNr)
    A.currentCluster = find(A.ClusterNr==ClustNr);
else
    A.currentCluster = find(A.ClusterNr==0);
end
AlSortGui('store',A);
AlSortGui('setClusterPanel',A);
AlSortGui('updatePlots',A,true);
end

function output = call_loadNSE(varargin)
output = {[]};
A = AlSortGui('retrieve');
if isempty(A) || ~isa(A,'AlSort')
    A = AlSort;
end
if nargin<2
    loadOption = 'normal';
else
    loadOption = varargin{1};
end
defaultExt = {'*.nse;*.Nse;*.NSE' 'Neuralynx SE-files (*.nse;*.Nse;*.NSE)';
    '*.*' 'All files (*.*)'};

switch loadOption
    case 'multi'
        dialogTitle = 'load MULTIPLE Neuralynx SE-files';
        param = {'multiselect','on'};
        [filename,pathname] = getFilePath(A,defaultExt,dialogTitle,param);
        if pathname==0;return;end
        A.WorkDir = pathname;
        A.FileNames = filename;
        for i=1:length(filename)
            NSE{i} = NLX_LoadNSE(fullfile(pathname,filename{i}),'full',1,[]);
            A.FileTimes(i,:) = NLX_getTimeRangeNSE(NSE{i});
        end
        NSE = NLX_mergeNSE(NSE{:});
    otherwise
        dialogTitle = 'load SINGLE Neuralynx SE-file';
        param = {'multiselect','off'};
        [filename,pathname] = getFilePath(A,defaultExt,dialogTitle,param);
        if pathname==0;return;end
        NSE = NLX_LoadNSE(fullfile(pathname,filename),'full',1,[]);
end

A.Data = NSE;
A = AlSort_initiate(A);

if isempty(A.Data.Path);return;end
%set(findobj('tag','AlSortGui_currentFile'),'string',A.Data.Path);

AlSortGui('setSortControl',A,'default');
AlSortGui('store',A);
AlSortGui('updatePlots',A,true);
end

function output = call_SaveNSE(varargin)
output = {[]};
A = AlSortGui('retrieve');
saveFile(A,'normal');
end

function output = call_SaveSpecialNSE(varargin)
output = {[]};
A = AlSortGui('retrieve');
saveFile(A,'special');
end

function output = call_loadNCS(varargin)
output = {[]};
A = AlSortGui('retrieve');
defaultExt = {'*.ncs;*.Ncs;*.NCS' 'Neuralynx continuous data file (*.ncs;*.Ncs;*.NCSV)';
    '*.*' 'All files (*.*)'};
dialogTitle = 'load Neuralynx continuous data file';
param = {'multiselect','off'};
[filename,pathname] = getFilePath(A,defaultExt,dialogTitle,param);
if pathname==0;return;end
A.DataAnalog = NLX_LoadNCS(fullfile(pathname,filename),'full',1,[]);
A.DataAnalog = NLX_NCS2NRD(A.DataAnalog);
if isempty(A.DataAnalog.Path);return;end

if isempty(A.TimeRange)
    A.TimeRange = [min(A.DataAnalog.TimeStamps) max(A.DataAnalog.TimeStamps)];
    A.TimeLim = A.TimeRange;
    A.TimeWin = A.TimeRange;
    A.DigitalRange = [0 1];
    A.FeatureWin = [0 1];
end

AlSortGui('store',A);
TargetAx = findobj('type','axes','tag','AlSortGui_LFPAxes');
if ~isempty(TargetAx)
    AlSortGui('plotAnalog',A,1,TargetAx);
end
end

function output = call_loadNEV(varargin)
output = {[]};
A = AlSortGui('retrieve');
defaultExt = {'*.nev;*.Nev;*.NEV' 'Neuralynx Eventfiles (*.nev;*.Nev;*.NEV)';
    '*.*' 'All files (*.*)'};
dialogTitle = 'load Neuralynx *.NEV data file';
param = {'multiselect','off'};
[filename,pathname] = getFilePath(A,defaultExt,dialogTitle,param);
A.Events = NLX_LoadNEV(fullfile(pathname,filename),'full',1,[]);
if isempty(A.Events.Path);return;end

if isempty(A.TimeRange)
    A.TimeRange = [min(A.Events.TimeStamps) max(A.Events.TimeStamps)];
    A.TimeLim = A.TimeRange;
    A.TimeWin = A.TimeRange;
    A.DigitalRange = [0 1];
    A.FeatureWin = [0 1];
end

AlSortGui('store',A);
AlSortGui('plotEvents',A);
end


function output = call_undo(varargin)
output = {[]};
A = AlSortGui('retrieve');
A = AlSort_SortBuffer(A,'BACK');
AlSortGui('store',A);
AlSortGui('setClusterPanel',A);
%AlSortGui('updateMarkedClusters',A,'ON');
AlSortGui('updatePlots',A,true);
end

function output = call_redo(varargin)
output = {[]};
A = AlSortGui('retrieve',AlSort);
A = AlSort_SortBuffer(A,'FORWARD');
AlSortGui('store',A);
AlSortGui('setClusterPanel',A);
% AlSortGui('updateMarkedClusters',A,'ON');
AlSortGui('updatePlots',A,true);
end

function output = call_returnSpiketimes(varargin)
output = {[]};
A = AlSortGui('retrieve');
[h,ClustLabel,isActive,isSelected] = AlSortGui('getClusterPanel',A);
listCluster = union(ClustLabel(isSelected),isActive);
[fDir,fName,fExt] = fileparts(A.Data.Path);
fprintf(1,'Folder      : %s\n',fDir);
fprintf(1,'File        : %s\n',[fName,fExt]);
fprintf(1,'Time window : %12.0f %12.0f\n',A.TimeWin(1),A.TimeWin(2));
for iC = 1:length(listCluster)
    cTime(1) = min(A.Data.TimeStamps(A.SortBuffer(:,A.ClusterNr==listCluster(iC),1) & A.Data.TimeStamps>A.TimeWin(1)));
    cTime(2) = max(A.Data.TimeStamps(A.SortBuffer(:,A.ClusterNr==listCluster(iC),1) & A.Data.TimeStamps<A.TimeWin(2)));
    fprintf(1,'Cluster #%2.0f first: %12.0f last: %12.0f\n',listCluster(iC),cTime(1),cTime(2));
end
end

function output = call_assignToBase(varargin)
output = {[]};
A = AlSortGui('retrieve');
assignin('base','A',A);
display(A);
end

function output = call_cdToFolder(varargin)
output = {[]};
A = AlSortGui('retrieve');
[fDir,fName,fExt] = fileparts(A.Data.Path);
if ~exist(fDir,'dir');return;end
cd(fDir);
end

function output = call_updatePlots(varargin)
output = {[]};
if length(varargin)>=1
    A = varargin{1};
else
    A = AlSortGui('retrieve');
end
if length(varargin)>=2
    UsePlotMode = varargin{2};
else
    UsePlotMode = true;
end
if isempty(A) || ~isa(A,'AlSort');return;end
updatePlots(A,UsePlotMode);
end

function output = call_closeAllPlot(varargin)
output = {[]};
close(findobj('tag','AlSortGui_StatPlot'));
set(findobj('tag','AlSortGuiMenu_StatsPlot'),'checked','off');
close(findobj('tag','AlSortGui_WaveformPlot'));
set(findobj('tag','AlSortGuiMenu_WaveformPlot'),'checked','off');
close(findobj('tag','AlSortGui_PCPlot'));
set(findobj('tag','AlSortGuiMenu_PCPlot'),'checked','off');
close(findobj('tag','AlSortGui_FeaturePlot'));
set(findobj('tag','AlSortGuiMenu_FeaturePlot'),'checked','off');
close(findobj('tag','AlSortGui_TimePlot'));
set(findobj('tag','AlSortGuiMenu_TimePlot'),'checked','off');
end

function output = call_arrangePlots(varargin)
output = {[]};
cf = findobj('tag','AlSortGui_StatPlot');
if ~isempty(cf);
    figure(cf);
    set(cf,'units','normalized','position',[0.25 0.55 0.20 0.4]);
end

cf = findobj('tag','AlSortGui_WaveformPlot');
if ~isempty(cf);
    figure(cf);
    set(cf,'units','normalized','position',[0.05 0.4 0.3 0.3]);
end

% cf = findobj('tag','AlSortGui_PCPlot');
% if ~isempty(cf);
%     figure(cf);
%     set(cf,'units','normalized','position',[0 0 0.3 0.3]);
% end

cf = findobj('tag','AlSortGui_FeaturePlot');
if ~isempty(cf);
    figure(cf);
    set(cf,'units','normalized','position',[0.5 0.20 0.4 0.7]);
end

cf = findobj('tag','AlSortGui_TimePlot');
if ~isempty(cf);
    figure(cf);
    set(cf,'units','normalized','position',[0.05 0.05 0.9 0.4],'selected','on');
end

% cf = findobj('tag','AlSortGui_SortControl');
% if ~isempty(cf);
%     figure(cf);
%     set(cf,'units','normalized','position',[0 0 0.3 0.3]);
% end
end

function output = call_plotsToFront(varargin)
output = {[]};
FigureTags = {'AlSortGui_StatPlot' 'AlSortGui_WaveformPlot' 'AlSortGui_PCPlot' 'AlSortGui_FeaturePlot' 'AlSortGui_TimePlot' 'AlSortGui_SortControl'};
for i=1:length(FigureTags)
    cf = findobj('tag',FigureTags{i});
    if ~isempty(cf);figure(cf);end
end
end


function output = call_plotTime(varargin)
output = {[]};
plotMenu = findobj('tag','AlSortGuiMenu_TimePlot');
if strcmp(get(plotMenu, 'Checked'),'on')
    set(plotMenu, 'Checked', 'off');
else
    set(plotMenu, 'Checked', 'on');
end
cF = findobj('type','figure','tag','AlSortGui_TimePlot');
if strcmp(get(plotMenu, 'Checked'),'on') && isempty(cF)
    cF = createTimePlot;
    A = AlSortGui('retrieve');
    if isa(A,'AlSort')
        AlSortGui('setTimePlot',A,'default');
        AlSortGui('updatePlots',A,true);
    end
elseif strcmp(get(plotMenu, 'Checked'),'off') && ~isempty(cF)
    close(cF);
end
output{1} = cF;
end

function output = call_plotFeature(varargin)
output = {[]};
plotMenu = findobj('tag','AlSortGuiMenu_FeaturePlot');
if strcmp(get(plotMenu, 'Checked'),'on')
    set(plotMenu, 'Checked', 'off');
else
    set(plotMenu, 'Checked', 'on');
end
cF = findobj('type','figure','tag','AlSortGui_FeaturePlot');
if strcmp(get(plotMenu, 'Checked'),'on') && isempty(cF)
    cF = createFeaturePlot;
    A = AlSortGui('retrieve');
    if isa(A,'AlSort')
        AlSortGui('setFeaturePlot',A);
        AlSortGui('updatePlots',A,true);
    end
elseif strcmp(get(plotMenu, 'Checked'),'off') && ~isempty(cF)
    close(cF);
end
output{1} = cF;
end

function output = call_plotWaveform(varargin)
output = {[]};
plotMenu = findobj('tag','AlSortGuiMenu_WaveformPlot');
if strcmp(get(plotMenu, 'Checked'),'on')
    set(plotMenu, 'Checked', 'off');
else
    set(plotMenu, 'Checked', 'on');
end
cF = findobj('type','figure','tag','AlSortGui_WaveformPlot');
if strcmp(get(plotMenu, 'Checked'),'on') && isempty(cF)
    cF = createWaveformPlot;
    A = AlSortGui('retrieve');
    if isa(A,'AlSort')
        setWaveformPlot(A);
        AlSortGui('updatePlots',A,true);
    end
elseif strcmp(get(plotMenu, 'Checked'),'off') && ~isempty(cF)
    close(cF);
end
output{1} = cF;
end

function output = call_plotStat(varargin)
output = {[]};
plotMenu = findobj('tag','AlSortGuiMenu_StatsPlot');
if strcmp(get(plotMenu, 'Checked'),'on')
    set(plotMenu, 'Checked', 'off');
else
    set(plotMenu, 'Checked', 'on');
end
cF = findobj('type','figure','tag','AlSortGui_StatPlot');
if strcmp(get(plotMenu, 'Checked'),'on') && isempty(cF)
    cF = createStatPlot;
    A = AlSortGui('retrieve');
    if isa(A,'AlSort')
        AlSortGui('updatePlots',A,true);
    end
elseif strcmp(get(plotMenu, 'Checked'),'off') && ~isempty(cF)
    close(cF);
end
output{1} = cF;
end

function output = call_plotEvents(varargin)
output = {[]};
A = varargin{1};
[userh,digh] = plotEvents(A);
end

function output = call_plotAnalog(varargin)
output = {[]};
A = varargin{1};
i = varargin{2};
TargetAx = varargin{3};
plotAnalog(A,i,TargetAx);
end

function output = call_closeSortControl(varargin)
output = {[]};
cF = findobj('type','figure','tag','AlSortGui_SortControl');
delete(cF);
removePath(AlSort);
end

function output = call_closeFeature(varargin)
output = {[]};
cF = findobj('type','figure','tag','AlSortGui_FeaturePlot');
set(findobj('tag','AlSortGuiMenu_FeaturePlot'),'checked','off');
delete(cF);
end

function output = call_closeTime(varargin)
output = {[]};
cF = findobj('type','figure','tag','AlSortGui_TimePlot');
set(findobj('tag','AlSortGuiMenu_TimePlot'),'checked','off');
delete(cF);
end

function output = call_closeStat(varargin)
output = {[]};
cF = findobj('type','figure','tag','AlSortGui_StatPlot');
set(findobj('tag','AlSortGuiMenu_StatsPlot'),'checked','off');
delete(cF);
end

function output = call_closeWaveform(varargin)
output = {[]};
cF = findobj('type','figure','tag','AlSortGui_WaveformPlot');
set(findobj('tag','AlSortGuiMenu_WaveformPlot'),'checked','off');
delete(cF);
end

function output = call_addCluster(varargin)
output = {[]};
A = AlSortGui('retrieve');
A = AlSort_addCluster(A);
AlSortGui('store',A);
AlSortGui('setClusterPanel',A);
% AlSortGui('updateMarkedClusters',A,'CURRENTON');
set(findobj('type','uicontrol','tag','AlSortGui_SortModeAdd'),'value',1);
end

function output = call_deleteCluster(varargin)
output = {[]};
A = AlSortGui('retrieve');
[h,ClustLabel,isActive,isSelected] = getClusterPanel(A);
A = AlSort_deleteCluster(A,ClustLabel(isSelected));
AlSortGui('store',A);
AlSortGui('setClusterPanel',A);
AlSortGui('updatePlots',A,true);
end

function output = call_mergeCluster(varargin)
output = {[]};
A = AlSortGui('retrieve');
[h,ClustLabel,isActive,isSelected] = AlSortGui('getClusterPanel',A);
A = AlSort_mergeCluster(A,ClustLabel(isSelected),isActive);
AlSortGui('store',A);
AlSortGui('setClusterPanel',A);
AlSortGui('updatePlots',A,true);
end

function output = call_copyCluster(varargin)
output = {[]};
A = AlSortGui('retrieve');
A = AlSort_copyCluster(A);
AlSortGui('store',A);
AlSortGui('setClusterPanel',A);
AlSortGui('updatePlots',A,true);
end


function output = call_selectAllCluster(varargin)
output = {[]};
A = AlSortGui('retrieve');
if isempty(A) || ~isa(A,'AlSort');return;end
AlSortGui('updateMarkedClusters',A,'ON');
AlSortGui('updatePlots',A,true);
end

function output = call_unselectAllCluster(varargin)
output = {[]};
A = AlSortGui('retrieve');
if isempty(A) || ~isa(A,'AlSort');return;end
AlSortGui('updateMarkedClusters',A,'OFF');
AlSortGui('updatePlots',A,true);
end

function output = call_computePCA(varargin)
output = {[]};
A = AlSortGui('retrieve');
ButtonName = questdlg('This PCA works ONLY in the current time window and on the waveforms of the currently selected/active clusters! RERUN PCA when you change these paremeter.', 'PCA analysis', ...
    'Proceed', 'cancel', 'cancel');
switch ButtonName
    case 'cancel';return;
end

[h,ClustLabel,isActive,isSelected] = AlSortGui('getClusterPanel',A);
ClusterNrs = union(ClustLabel(isSelected),isActive);
[WaveFormSampleIndex,OK] = listdlg('PromptString','Select Waveformsamples','Name','PCA',...
    'SelectionMode','multiple',...
    'ListString',cellstr(num2str([1:32]')), ...
    'InitialValue',[4:20]);
if OK==0, return;end
A = AlSort_computePrincipalComponents(A,ClusterNrs,true,WaveFormSampleIndex);
AlSortGui('store',A);
end

function output = call_updateLim(varargin)
output = {[]};
A = AlSortGui('retrieve');
AlSortGui_setFeaturePlotLimits(A);
%         A = AlSortGui('retrieve');
%         A = AlSortGui_setFeaturePlotLimits(A,'SPREAD',3);
end

function output = call_slideTime(varargin)
output = {[]};
A = AlSortGui('retrieve');
currLim = A.TimeLim;
currWidth = diff(currLim);
currCenter = currLim(1) + currWidth*0.5;
hSlide = findobj('type','uicontrol','tag','AlSortGui_TimeShiftSlider');
currStep = get(hSlide,'Value') - currCenter;
A.TimeLim = currLim + currStep;
AlSortGui('store',A);
AlSortGui('setTimePlot',A);
end

function output = call_zoomInTime(varargin)
output = {[]};
A = AlSortGui('retrieve');
Width = diff(A.TimeLim);
OldPos = A.TimeLim(1) + Width*0.5;
Width = Width*0.25;
A.TimeLim = [OldPos-Width*0.5 OldPos+Width*0.5];
AlSortGui('store',A);
AlSortGui('setTimePlot',A);
end

function output = call_zoomOutTime(varargin)
output = {[]};
A = AlSortGui('retrieve');
Width = diff(A.TimeLim);
OldPos = A.TimeLim(1) + Width*0.5;
Width = Width*2;
A.TimeLim = [OldPos-Width*0.5 OldPos+Width*0.5];
AlSortGui('store',A);
AlSortGui('setTimePlot',A);
end

function output = call_getFeatures(varargin)
output = {[]};
for i=1:3
    hFeatList = findobj('type','uicontrol','tag',sprintf('AlSortGui_listF%1.0f',i));
    FeatList = get(hFeatList,'string');
    FeatNr = get(hFeatList,'value');
    cFeatures{i} = FeatList{FeatNr};
    if strcmp(FeatList{FeatNr}(1),'#')
        cFeatures{i} = str2double(FeatList{FeatNr}(2:end));
    else
        cFeatures{i} = FeatList{FeatNr};
    end
end
output{1} = cFeatures;
end

function output = call_loadFeatureSettings(varargin)
output = {[]};
featureString = varargin{1};
featureString = textscan(featureString,'%s%s%s','delimiter',' x ');
for i=1:3
    hFeatList = findobj('type','uicontrol','tag',sprintf('AlSortGui_listF%1.0f',i));
    FeatList = get(hFeatList,'string');
    if ~isnan(str2double(featureString{1}{i}))
        cFeatListString = ['#' featureString{1}{i}];
    else
        cFeatListString = featureString{1}{i};
    end
    cFeatNr = find(strcmp(FeatList,cFeatListString));
    if ~isempty(cFeatNr)
        set(hFeatList,'value',cFeatNr)
    else
        errordlg(sprintf('Could not find feature "%s"!',cFeatListString));
    end
end

AlSortGui('resetFeatureLimits','F1');
AlSortGui('resetFeatureLimits','F2');
AlSortGui('resetFeatureLimits','F3');
AlSortGui('updatePlots');
end


function output = call_getFeatureLimits(varargin)
output = {[]};
output{1}(1) = str2double(get(findobj('tag','AlSortGui_loLimF1'),'string'));
output{1}(2) = str2double(get(findobj('tag','AlSortGui_hiLimF1'),'string'));
output{2}(1) = str2double(get(findobj('tag','AlSortGui_loLimF2'),'string'));
output{2}(2) = str2double(get(findobj('tag','AlSortGui_hiLimF2'),'string'));
output{3}(1) = str2double(get(findobj('tag','AlSortGui_loLimF3'),'string'));
output{3}(2) = str2double(get(findobj('tag','AlSortGui_hiLimF3'),'string'));
output{4}(1) = str2double(get(findobj('tag','AlSortGui_loLimTime'),'string'));
output{4}(2) = str2double(get(findobj('tag','AlSortGui_hiLimTime'),'string'));
end

function output = call_updateFeatureLimits(varargin)
output = {[]};
A = AlSortGui('retrieve');
[limF1,limF2,limF3,limTime] = AlSortGui('getFeatureLimits');
A.FeatureLim(1,:) = limF1;
A.FeatureLim(2,:) = limF2;
A.FeatureLim(3,:) = limF3;
A.TimeLim = limTime;
AlSortGui('store',A);
AlSortGui('setTimePlot',A);
AlSortGui('setFeaturePlot',A);
%         AlSortGui('setWaveformPlot',A);
%         AlSortGui('setPCPlot',A);
end

function output = call_resetFeatureLimits(varargin)
output = {[]};
featString = varargin{1};
A = AlSortGui('retrieve');
switch featString
    case 'F1';AlSortGui('setFeatureLimits','F1',A.DigitalRange);
    case 'F2';AlSortGui('setFeatureLimits','F2',A.DigitalRange);
    case 'F3';AlSortGui('setFeatureLimits','F3',A.DigitalRange);
    case 'Time';AlSortGui('setFeatureLimits','Time',A.TimeRange);
end
AlSortGui('updateFeatureLimits');
end

function output = call_setFeatureLimits(varargin)
output = {[]};
% AlSortGui_('setFeatureLimits','F2',ylim);
set(findobj('tag',sprintf('AlSortGui_loLim%s',varargin{1})),'string',sprintf('%1.0f',varargin{2}(1)));
set(findobj('tag',sprintf('AlSortGui_hiLim%s',varargin{1})),'string',sprintf('%1.0f',varargin{2}(2)));
end

function output = call_setSortControl(varargin)
output = {[]};
A = varargin{1};
if length(varargin)>1
    options = varargin{2};
else
    options = '';
end
A = setSortControl(A,options);
output{1} = A;
end

function output = call_setTimePlot(varargin)
output = {[]};
A = varargin{1};
if length(varargin)>1
    options = varargin{2};
else
    options = '';
end
A = setTimePlot(A,options);
output{1} = A;
end

function output = call_setFeaturePlot(varargin)
output = {[]};
A = varargin{1};
if length(varargin)>1
    options = varargin{2};
else
    options = '';
end
A = setFeaturePlot(A,options);
output{1} = A;
end

function output = call_setClusterPanel(varargin)
output = {[]};
A = varargin{1};
h = setClusterPanel(A);
output{1} = h;
end

function output = call_getClusterPanel(varargin)
output = {[]};
A = varargin{1};
[h,ClustLabel,isActive,isSelected] = getClusterPanel(A);
output{1} = h;
output{2} = ClustLabel;
output{3} = isActive;
output{4} = isSelected;
end


function output = call_store(varargin)
output = {[]};
A = varargin{1};
cF = findobj('type','figure','tag','AlSortGui_SortControl');
if ~isa(A,'AlSort') || isempty(A.Data)
    set(cF,'Name','empty','userdata',A);
else
    [fDir,fName,fExt] = fileparts(A.Data.Path);
    set(cF,'Name',[fName fExt],'userdata',A);
end
end

function output = call_retrieve(varargin)
output = {[]};
cF = findobj('type','figure','tag','AlSortGui_SortControl');
output{1} = get(cF,'userdata');
end

function output = call_plotClusterData(varargin)
output = {[]};
A = varargin{1};
ClusterToPlot = varargin{2};
FeatureName = varargin{3};
TargetAx = varargin{4};
HideOtherCluster = varargin{5};
UseTimeWin = varargin{6};
currClusterPlot = plotClusterData(A,ClusterToPlot,FeatureName,TargetAx,HideOtherCluster,UseTimeWin);
end

function output = call_setActiveCluster(varargin)
output = {[]};
A = AlSortGui('retrieve');
GetActiveFromGui = varargin{1};
% this function is called by switching the radiobuttons
[h,ClustLabel,isActive,isSelected] = AlSortGui('getClusterPanel',A);
if GetActiveFromGui
    A = AlSort_setCurrentCluster(A,isActive);
else
    isActive = A.ClusterNr(A.currentCluster);
    set(findobj('tag',sprintf('AlSortGui_C%1.0f_Active',isActive)),'value',1);
end
% update highlighting of cluster label
for iC = 1:length(ClustLabel)
    if ClustLabel(iC)==isActive;cColorIndex=2;else cColorIndex=1;end
    set(findobj('tag',sprintf('AlSortGui_C%1.0f_Label',ClustLabel(iC))),'foregroundcolor',A.ClusterColor(A.ClusterNr(iC)+1,:,cColorIndex))
end
AlSortGui('store',A);
AlSortGui('updatePlots',A,true);
end

function output = call_plotTimeWindow(varargin)
output = {[]};
A = varargin{1};
FigH = varargin{2};
A = plotTimeWindow(A,FigH);
output{1} = A;
end

function output = call_setTimeWindow(varargin)
output = {[]};
cTimeFig = findobj('tag','AlSortGui_TimePlot');
if isempty(cTimeFig)
    warndlg('Create Feature vs Time figure first!','set a time window');
    return;
end

A = AlSortGui('retrieve');

cTimeAxes = findobj('parent',cTimeFig,'tag','AlSortGui_TimeAxes');
axes(cTimeAxes);

% set timemarkers interactively (using ginput) and store them in A.TimeWin
cTimeWin = [];
doSelect = true;
while doSelect
    [xi,yi,button] = ginput(1);
    if button==1
        cTimeWin = cat(2,cTimeWin,xi);
        A.TimeWin = sort(cTimeWin);
        AlSortGui('plotTimeWindow',A,cTimeAxes);
    else
        doSelect = false;
    end
end

AlSortGui('store',A);
AlSortGui('updatePlots',A);
AlSortGui('setClusterPanel',A);

figure(findobj('tag','AlSortGui_SortControl'));
end

function output = call_resetTimeWindow(varargin)
output = {[]};
A = AlSortGui('retrieve');
A.TimeWin = A.TimeRange;
AlSortGui('store',A);
AlSortGui('updatePlots',A);
AlSortGui('setClusterPanel',A);
end

function output = call_spreadTimeWindow(varargin)
output = {[]};
A = AlSortGui('retrieve');
A.TimeLim = A.TimeWin;
AlSortGui('store',A);
%         AlSortGui('updatePlots',A);
AlSortGui('setTimePlot',A);
end


function output = call_setWaveformNum(varargin)
output = {[]};
A = AlSortGui('retrieve');
title = 'set waveform N ...';
descriptionText = '';
editStrings = {'waveform N'};
defaultStrings = {num2str(A.nWaveform)};
responseCode = createEditDialog(title,descriptionText,editStrings,defaultStrings);
A.nWaveform = str2double(responseCode{1});
AlSortGui('store',A);
AlSortGui('setClusterPanel',A);
AlSortGui('updatePlots',A,true);
end

function output = call_cutCluster(varargin)
output = {[]};
A = AlSortGui('retrieve');
%         responseCode = createClusterCutDialog({'Feature vs Time','Feature vs Feature','Waveform'});
responseCode = varargin{1};
switch responseCode
    case 'time'
        cutFig = findobj('tag','AlSortGui_TimePlot');
        if isempty(cutFig)
            cutFig = AlSortGui('plotTime');
        end
        cutAxes = findobj('parent',cutFig,'tag','AlSortGui_TimeAxes');
        axes(cutAxes);
        PolygonNodes = selectPolygon(A);
        cFeatures = getAxesFeatures(A,'TimePlot');
        XYZFeature = cFeatures(1:2);
        UseTimeWin = false;
        SpaceRotation = [];
        A = cutCluster(A,XYZFeature,PolygonNodes,UseTimeWin,SpaceRotation);
    case 'feature'
        cutFig = findobj('tag','AlSortGui_FeaturePlot');
        if isempty(cutFig)
            cutFig = AlSortGui('plotFeature');
        end
        cutAxes = findobj('parent',cutFig,'tag','AlSortGui_FeatureAxes');
        axes(cutAxes);
        PolygonNodes = selectPolygon(A);
        cFeatures = getAxesFeatures(A,'FeaturePlot');
        XYZFeature = cFeatures;
        UseTimeWin = true;
        SpaceRotation(:,1) =  get(cutAxes,'CameraPosition')';
        SpaceRotation(:,2) =  get(cutAxes,'CameraTarget')';
        SpaceRotation(:,3) =  get(cutAxes,'CameraUpVector')';
        A = cutCluster(A,XYZFeature,PolygonNodes,UseTimeWin,SpaceRotation);
    case 'wave'
        cutFig = findobj('tag','AlSortGui_WaveformPlot');
        if isempty(cutFig)
            cutFig = AlSortGui('plotWaveform');
        end
        cutAxes = findobj('parent',cutFig,'tag','AlSortGui_WaveformAxes');
        axes(cutAxes);
        WaveformHull = selectWaveform(A);
        UseTimeWin = true;
        SpaceRotation = [];
        A = cutCluster(A,[1:32],WaveformHull,UseTimeWin,SpaceRotation);
end

AlSortGui('store',A);
AlSortGui('updatePlots',A);
AlSortGui('setClusterPanel',A);
end

function output = call_cutTime(varargin)
output = {[]};
A = AlSortGui('retrieve');
axes(findobj('tag','AlSortGui_TimeAxes'));
Pt = AlSortGui_selectPolygon(A);
cFeatures = AlSortGui_getAxesFeatures(A,'TimePlot');
A = AlSortGui_cutCluster(A,cFeatures(1:2),Pt,false);
AlSortGui('store',A);
AlSortGui('setClusterPanel',A);
AlSortGui('updatePlots',A,true);
end

function output = call_cutFeature(varargin)
output = {[]};
A = AlSortGui('retrieve');
axes(findobj('tag','AlSortGui_FeatureAxes'));
Pt = AlSortGui_selectPolygon(A);
cFeatures = AlSortGui_getAxesFeatures(A,'FeaturePlot');
cAxes = findobj('parent',get(hObject,'parent'),'type','axes','tag','AlSortGui_FeatureAxes');
SpaceRotation(:,1) =  get(cAxes,'CameraPosition')';
SpaceRotation(:,2) =  get(cAxes,'CameraTarget')';
SpaceRotation(:,3) =  get(cAxes,'CameraUpVector')';
A = AlSortGui('cutCluster',A,cFeatures,Pt,true,SpaceRotation);
AlSortGui('store',A);
% A = AlSortGui_setCurrentClusters(A,true);
AlSortGui('setClusterPanel',A);
AlSortGui('updatePlots',A,true);
end

function output = call_applyBoundaries(varargin)
output = {[]};
A = AlSortGui('retrieve');
[h,ClustLabel,isActive,isSelected] = AlSortGui('getClusterPanel',A);
index = any(A.SortBuffer(:,isSelected,1),2);
WaveformHull = NLX_getBoundaries(A.Data,index);
UseTimeWin = true;
SpaceRotation = [];
A = cutCluster(A,[1:32],flipud(WaveformHull'),UseTimeWin,SpaceRotation);
AlSortGui('store',A);
AlSortGui('updatePlots',A);
AlSortGui('setClusterPanel',A);
end

function output = call_updateMarkedClusters(varargin)
output = {[]};
A = varargin{1};
option = varargin{2};
[h,ClustLabel,isActive,isSelected] = AlSortGui('getClusterPanel',A);
%             isCurr = false(size(isMarked));
%             isCurr(A.ClusterNr(any(A.SortBuffer(:,:,1),1))+1) = true;
switch option
    case 'OFF'
        setMarkedClusters(A,[]);
    case 'ON'
        setMarkedClusters(A,ClustLabel);
        %                 case 'CHECKON'
        %                     AlSortGui_setMarkedClusters(A,isMarked&isCurr);
        %                 case 'CURRENTON'
        %                     isMarked(A.ClusterNr(A.currentCluster)+1) = true;
        %                     AlSortGui_setMarkedClusters(A,isMarked);
        %                 case 'CURRENTOFF'
        %                     isMarked(A.ClusterNr(A.currentCluster)+1) = false;
        %                     AlSortGui_setMarkedClusters(A,isMarked);
    otherwise
        error('AlSortGui_updateMarkedClusters: Don''t know option "%s"',option);
end
end

function output = call_moveTimeWindowForward(varargin)
output = {[]};
A = AlSortGui('retrieve');
stepSize = get(findobj('tag','AlSortGui_moveTimeWindowStep'),'string');
stepSize = str2double(stepSize);
A.TimeWin = A.TimeWin + stepSize;
AlSortGui('store',A);
AlSortGui('updatePlots',A);
AlSortGui('setClusterPanel',A);
end

function output = call_moveTimeWindowBackward(varargin)
output = {[]};
A = AlSortGui('retrieve');
stepSize = get(findobj('tag','AlSortGui_moveTimeWindowStep'),'string');
stepSize = str2double(stepSize);
A.TimeWin = A.TimeWin - stepSize;
AlSortGui('store',A);
AlSortGui('updatePlots',A);
AlSortGui('setClusterPanel',A);
end


%#########################################################################
%#########################################################################
%#########################################################################


function hFig = createSortControl
guiColor = [0 0 0];
pushButtonColor = [.3 .3 .3];
hFig = figure('menubar','none','numbertitle','off','name','AlSort3','tag','AlSortGui_SortControl','color',guiColor, ...
    'units','centimeters','position',[3 10 10 12],'resize','off', ...
    'ButtonDownFcn','AlSortGui(''plotsToFront'');', ...
    'CloseRequestFcn','AlSortGui(''closeSortControl'');');

% make menus
hMainMenu = uimenu('parent',hFig,'label','File','tag','MainMenuFile','callback','');
hMenu = uimenu('parent',hMainMenu,'label','Load *.Nse','tag','AlGui_Menu_LoadNSE','callback','AlSortGui(''loadNSE'');');
hMenu = uimenu('parent',hMainMenu,'label','Load *.Nev','tag','AlSortGui_Menu_LoadNev','callback','AlSortGui(''loadNEV'');');
hMenu = uimenu('parent',hMainMenu,'label','Load *.Ncs','tag','AlSortGui_Menu_LoadNcs','callback','AlSortGui(''loadNCS'');');
hMenu = uimenu('parent',hMainMenu,'label','Load multi *.Nse','tag','AlGui_Menu_LoadMultiNSE','callback','AlSortGui(''loadNSE'',''multi'');','Separator','on');
hMenu = uimenu('parent',hMainMenu,'label','Save *.Nse','tag','AlGui_Menu_SaveNSE','callback','AlSortGui(''SaveNSE'');','Separator','on');
hMenu = uimenu('parent',hMainMenu,'label','Save special *.nse','tag','AlGui_Menu_SaveSpecialNSE','callback','AlSortGui(''SaveSpecialNSE'');');
hMenu = uimenu('parent',hMainMenu,'label','AutoSave Report','tag','AlGui_Menu_AutoSaveReport','callback','','Separator','on');

hMainMenu = uimenu('parent',hFig,'label','Edit','tag','MainMenuEdit','callback','');
hMenu = uimenu('parent',hMainMenu,'label','undo','tag','AlSortGuiMenu_undo','callback','AlSortGui(''undo'');');
hMenu = uimenu('parent',hMainMenu,'label','redo','tag','AlSortGuiMenu_redo','callback','AlSortGui(''redo'');');
hMenu = uimenu('parent',hMainMenu,'label','to base workspace','tag','AlSortGuiMenu_assignToBase','callback','AlSortGui(''assignToBase'');','Separator','on');
hMenu = uimenu('parent',hMainMenu,'label','create report','tag','AlGui_Menu_createReport','callback','');
hMenu = uimenu('parent',hMainMenu,'label','return first/last spikes','tag','AlGui_Menu_returnSpiketimes','callback','AlSortGui(''returnSpiketimes'');');
hMenu = uimenu('parent',hMainMenu,'label','cd to data folder','tag','AlGui_Menu_cdtoFolder','callback','AlSortGui(''cdToFolder'');');
hMenu = uimenu('parent',hMainMenu,'label','set Waveform N','tag','AlGui_Menu_setWaveformNum','callback','AlSortGui(''setWaveformNum'');');

hMainMenu = uimenu('parent',hFig,'label','Cluster','tag','MainMenuCluster','callback','');
hMenu = uimenu('parent',hMainMenu,'label','add','tag','AlSortGuiMenu_addCluster','callback','AlSortGui(''addCluster'');');
hMenu = uimenu('parent',hMainMenu,'label','copy','tag','AlSortGuiMenu_copyCluster','callback','AlSortGui(''copyCluster'');');
hMenu = uimenu('parent',hMainMenu,'label','merge','tag','AlSortGuiMenu_mergeCluster','callback','AlSortGui(''mergeCluster'');');
hMenu = uimenu('parent',hMainMenu,'label','delete','tag','AlSortGuiMenu_deleteCluster','callback','AlSortGui(''deleteCluster'');');
hMenu = uimenu('parent',hMainMenu,'label','select all','tag','AlSortGuiMenu_selectAllCluster','callback','AlSortGui(''selectAllCluster'');','Separator','on');
hMenu = uimenu('parent',hMainMenu,'label','unselect all','tag','AlSortGuiMenu_unselectAllCluster','callback','AlSortGui(''unselectAllCluster'');');
hMenu = uimenu('parent',hMainMenu,'label','apply boundaries','tag','AlSortGuiMenu_applyBoundaries','callback','AlSortGui(''applyBoundaries'');','Separator','on');

hMainMenu = uimenu('parent',hFig,'label','Analyse','tag','MainMenuAnalyse','callback','');
hMenu = uimenu('parent',hMainMenu,'label','Principal Components','tag','AlSortGuiMenu_PrinComp','callback','AlSortGui(''computePCA'');');
hMenu = uimenu('parent',hMainMenu,'label','Stats','tag','AlSortGuiMenu_StatsPlot','callback','AlSortGui(''plotStat'');','Separator','on');

% hMainMenu = uimenu('parent',hFig,'label','Settings','tag','MainMenuAnalyse','callback','');
% hMenu = uimenu('parent',hMainMenu,'label','load figure settings ...','tag','AlSortGuiMenu_loadFigureSettings','callback','AlSortGui(''loadFigureSettings'');');
% hMenu = uimenu('parent',hMainMenu,'label','save figure settings ...','tag','AlSortGuiMenu_saveFigureSettings','callback','AlSortGui(''saveFigureSettings'');');

hMainMenu = uimenu('parent',hFig,'label','Features','tag','MainMenuAnalyse','callback','');
featureSettings = { ...
    '8 x 14 x 17'; ...
    '8 x 12 x 17'; ...
    '8 x 12 x 14'; ...
    '8 x 14 x 20'; ...
    '8 x 12 x 20'; ...
    '8 x 17 x 20'; ...
    '8 x 4 x 20'; ...
    '4 x 15 x 25'; ...
    '3 x 5 x 7'; ...
    '12 x 14 x 16'; ...
    '15 x 17 x 19'; ...
    '20 x 23 x 25'; ...
    'Energy x 4 x 20'
    'Energy x 14 x 17'
    'Energy x 17 x 23'};
hMenu = uimenu('parent',hMainMenu,'label',featureSettings{1},'tag','AlSortGuiMenu_loadFeatureSettings','callback',sprintf('AlSortGui(''loadFeatureSettings'',''%s'');',featureSettings{1}));
for i=2:size(featureSettings,1)
    hMenu = uimenu('parent',hMainMenu,'label',featureSettings{i},'tag','AlSortGuiMenu_saveFeatureSettings','callback',sprintf('AlSortGui(''loadFeatureSettings'',''%s'');',featureSettings{i}));
end

hMainMenu = uimenu('parent',hFig,'label','Plots','tag','MainMenuPlots','callback','');
hMenu = uimenu('parent',hMainMenu,'label','refresh','tag','AlSortGuiMenu_refreshPlot','callback','AlSortGui(''updatePlots'');');
hMenu = uimenu('parent',hMainMenu,'label','arrange','tag','AlSortGuiMenu_arrangePlot','callback','AlSortGui(''arrangePlots'');');
hMenu = uimenu('parent',hMainMenu,'label','close all','tag','AlSortGuiMenu_closeAllPlot','callback','AlSortGui(''closeAllPlot'');');
hMenu = uimenu('parent',hMainMenu,'label','Feature vs Time','tag','AlSortGuiMenu_TimePlot','callback','AlSortGui(''plotTime'');','Separator','on');
hMenu = uimenu('parent',hMainMenu,'label','Feature vs Feature','tag','AlSortGuiMenu_FeaturePlot','callback','AlSortGui(''plotFeature'');');
hMenu = uimenu('parent',hMainMenu,'label','Waveforms','tag','AlSortGuiMenu_WaveformPlot','callback','AlSortGui(''plotWaveform'');');

% make panels
cPosInCm = getPosInCM(hFig);
PanelSpacing = 0.1;
cPanelGrid1 = [6 1];
dPanelPos1 = getObjPos(cPosInCm([4 3]),cPanelGrid1(1),cPanelGrid1(2),[PanelSpacing PanelSpacing],[PanelSpacing PanelSpacing],[0.1 0.2 0.1 0.25 0.25 0.1],[1],100,100);
cPanelGrid2 = [6 2];
dPanelPos2 = getObjPos(cPosInCm([4 3]),cPanelGrid2(1),cPanelGrid2(2),[PanelSpacing PanelSpacing],[PanelSpacing PanelSpacing],[0.1 0.2 0.1 0.25 0.25 0.1],[0.5 0.5],100,100);
cPanelGrid3 = [5 2];
dPanelPos3 = getObjPos(cPosInCm([4 3]),cPanelGrid3(1),cPanelGrid3(2),[PanelSpacing PanelSpacing],[PanelSpacing PanelSpacing],[0.1 0.3 0.25 0.25 0.1],[0.5 0.5],100,100);
cPanelGrid4 = [6 3];
dPanelPos4 = getObjPos(cPosInCm([4 3]),cPanelGrid4(1),cPanelGrid4(2),[PanelSpacing PanelSpacing],[PanelSpacing PanelSpacing],[0.1 0.2 0.1 0.25 0.25 0.1],ones(1,3).*(1/3),100,100);

hButtonPanel = uibuttongroup('parent',hFig,'units','centimeter','position',[dPanelPos1(1,1,1) dPanelPos1(1,2,1) dPanelPos1(1,3,1) dPanelPos1(1,4,1)], ...
    'title','Cluster-panel','tag','AlSortGui_ClusterPanel', ...
    'foregroundcolor','y','backgroundcolor',guiColor);

hButtonPanel = uibuttongroup('parent',hFig,'units','centimeter','position',[dPanelPos2(2,1,1) dPanelPos2(2,2,1) dPanelPos2(2,3,1) dPanelPos2(2,4,1)], ...
    'title','Plot-Mode','tag','AlSortGui_PlotMode','SelectionChangeFcn','AlSortGui(''updatePlots'');', ...
    'foregroundcolor','y','backgroundcolor',guiColor);
cObjPos = getObjPos([1 1],3,1,[0.01 0.1],[0.01 0.01],ones(1,3).*(1/3),[1],100,100);
uicontrol('style','radiobutton','parent',hButtonPanel,'units','normalized','position',cObjPos(1,:,1),'string','active only','tag','AlSortGui_PlotModeOnly','foregroundcolor','w','backgroundcolor',guiColor);
uicontrol('style','radiobutton','parent',hButtonPanel,'units','normalized','position',cObjPos(2,:,1),'string','0 + active','tag','AlSortGui_PlotModeZero','foregroundcolor','w','backgroundcolor',guiColor);
uicontrol('style','radiobutton','parent',hButtonPanel,'units','normalized','position',cObjPos(3,:,1),'string','# + active','tag','AlSortGui_PlotModeAny','foregroundcolor','w','backgroundcolor',guiColor);

hButtonPanel = uibuttongroup('parent',hFig,'units','centimeter','position',[dPanelPos3(2,1,2) dPanelPos3(2,2,2) dPanelPos3(2,3,2) dPanelPos3(2,4,2)], ...
    'title','Sort-Mode','tag','AlSortGui_SortMode', ...
    'foregroundcolor','y','backgroundcolor',guiColor);
cObjPos = getObjPos([1 1],5,1,[0.01 0.1],[0.01 0.01],ones(1,5).*(1/5),[1],100,100);
uicontrol('style','radiobutton','parent',hButtonPanel,'units','normalized','position',cObjPos(1,:,1),'string','cut','tag','AlSortGui_SortModeExclusion','foregroundcolor','w','backgroundcolor',guiColor,'tooltipstring','exclude OUTSIDE polygon');
uicontrol('style','radiobutton','parent',hButtonPanel,'units','normalized','position',cObjPos(2,:,1),'string','split','tag','AlSortGui_SortModeSplit','foregroundcolor','w','backgroundcolor',guiColor,'tooltipstring','Split excluded waveforms to a NEW cluster.');
uicontrol('style','radiobutton','parent',hButtonPanel,'units','normalized','position',cObjPos(3,:,1),'string','add','tag','AlSortGui_SortModeAdd','foregroundcolor','w','backgroundcolor',guiColor,'tooltipstring','Add cuts to ACTIVE cluster.');
% uicontrol('style','radiobutton','parent',hButtonPanel,'units','normalized','position',cObjPos(4,:,1),'string','cut in TW','tag','AlSortGui_SortModeExclusionTW','foregroundcolor','w','backgroundcolor',guiColor,'tooltipstring','Apply the cut only IN the time window.');
uicontrol('style','checkbox','parent',hButtonPanel,'units','normalized','position',cObjPos(4,:,1),'string','time window only','tag','AlSortGui_SortModeExclusionTW','foregroundcolor','c','backgroundcolor',guiColor,'tooltipstring','operate only IN time window.');
uicontrol('style','checkbox','parent',hButtonPanel,'units','normalized','position',cObjPos(5,:,1),'string','inverse','tag','AlSortGui_SortInverseSelection','foregroundcolor','c','backgroundcolor',guiColor,'tooltipstring','exclude INSIDE polygon');

hButtonPanel = uibuttongroup('parent',hFig,'units','centimeter','position',[dPanelPos2(3,1,1) dPanelPos2(3,2,1) dPanelPos2(3,3,1) dPanelPos2(3,4,1)], ...
    'title','"0" color','tag','AlSortGui_0ColorMode', ...
    'foregroundcolor','y','backgroundcolor',guiColor);
cObjPos = getObjPos([1 1],1,3,[0.01 0.01],[0.01 0.01],[1],ones(1,3).*(1/3),100,100);
uicontrol('style','radiobutton','parent',hButtonPanel,'units','normalized','position',cObjPos(1,:,1),'string','auto','tag','AlSortGui_ZeroColorAuto','foregroundcolor','w','backgroundcolor',guiColor);
uicontrol('style','radiobutton','parent',hButtonPanel,'units','normalized','position',cObjPos(1,:,2),'string','Lo','tag','AlSortGui_ZeroColorLo','foregroundcolor','w','backgroundcolor',guiColor);
uicontrol('style','radiobutton','parent',hButtonPanel,'units','normalized','position',cObjPos(1,:,3),'string','Hi','tag','AlSortGui_ZeroColorHi','foregroundcolor','w','backgroundcolor',guiColor);

hButtonPanel = uibuttongroup('parent',hFig,'units','centimeter','position',[dPanelPos1(4,1,1) dPanelPos1(4,2,1) dPanelPos1(4,3,1) dPanelPos1(4,4,1)], ...
    'title','Features','tag','AlSortGui_Features', ...
    'foregroundcolor','y','backgroundcolor',guiColor);
cObjPos = getObjPos([1 1],4,4,[0.01 0.01],[0.01 0.01],ones(1,4).*(1/4),[0.1 0.2 0.35 0.35],100,100);
RowNr = 1;
uicontrol('style','pushbutton','string','F1','tag','AlSortGui_resetLimF1','units','normalized','position',[cObjPos(RowNr,1,1) cObjPos(RowNr,2,1) cObjPos(RowNr,3,1) cObjPos(RowNr,4,1)],'foregroundcolor','y','backgroundcolor',pushButtonColor,'parent',hButtonPanel,'HorizontalAlignment','center','callback','AlSortGui(''resetFeatureLimits'',''F1'');','tooltipstring','RESET plot limit of F1');
uicontrol('style','popupmenu','string','featurenames','tag','AlSortGui_listF1','units','normalized','position',[cObjPos(RowNr,1,2) cObjPos(RowNr,2,2) cObjPos(RowNr,3,2) cObjPos(RowNr,4,2)],'backgroundcolor','w','parent',hButtonPanel,'callback','AlSortGui(''updatePlots'');','tooltipstring','choose F1 from a set of features');
uicontrol('style','edit','string','','tag','AlSortGui_loLimF1','units','normalized','position',[cObjPos(RowNr,1,3) cObjPos(RowNr,2,3) cObjPos(RowNr,3,3) cObjPos(RowNr,4,3)],'backgroundcolor','w','parent',hButtonPanel,'callback','AlSortGui(''updateFeatureLimits'');','tooltipstring','set LOWER limit of F1 plot range');
uicontrol('style','edit','string','','tag','AlSortGui_hiLimF1','units','normalized','position',[cObjPos(RowNr,1,4) cObjPos(RowNr,2,4) cObjPos(RowNr,3,4) cObjPos(RowNr,4,4)],'backgroundcolor','w','parent',hButtonPanel,'callback','AlSortGui(''updateFeatureLimits'');','tooltipstring','set UPPER limit of F1 plot range');
RowNr = 2;
uicontrol('style','pushbutton','string','F2','tag','AlSortGui_resetLimF2','units','normalized','position',[cObjPos(RowNr,1,1) cObjPos(RowNr,2,1) cObjPos(RowNr,3,1) cObjPos(RowNr,4,1)],'foregroundcolor','y','backgroundcolor',pushButtonColor,'parent',hButtonPanel,'HorizontalAlignment','center','callback','AlSortGui(''resetFeatureLimits'',''F2'');','tooltipstring','RESET plot limit of F2');
uicontrol('style','popupmenu','string','featurenames','tag','AlSortGui_listF2','units','normalized','position',[cObjPos(RowNr,1,2) cObjPos(RowNr,2,2) cObjPos(RowNr,3,2) cObjPos(RowNr,4,2)],'backgroundcolor','w','parent',hButtonPanel,'callback','AlSortGui(''updatePlots'');','tooltipstring','choose F2 from a set of features');
uicontrol('style','edit','string','','tag','AlSortGui_loLimF2','units','normalized','position',[cObjPos(RowNr,1,3) cObjPos(RowNr,2,3) cObjPos(RowNr,3,3) cObjPos(RowNr,4,3)],'backgroundcolor','w','parent',hButtonPanel,'callback','AlSortGui(''updateFeatureLimits'');','tooltipstring','set LOWER limit of F2 plot range');
uicontrol('style','edit','string','','tag','AlSortGui_hiLimF2','units','normalized','position',[cObjPos(RowNr,1,4) cObjPos(RowNr,2,4) cObjPos(RowNr,3,4) cObjPos(RowNr,4,4)],'backgroundcolor','w','parent',hButtonPanel,'callback','AlSortGui(''updateFeatureLimits'');','tooltipstring','set UPPER limit of F2 plot range');
RowNr = 3;
uicontrol('style','pushbutton','string','F3','tag','AlSortGui_resetLimF3','units','normalized','position',[cObjPos(RowNr,1,1) cObjPos(RowNr,2,1) cObjPos(RowNr,3,1) cObjPos(RowNr,4,1)],'foregroundcolor','y','backgroundcolor',pushButtonColor,'parent',hButtonPanel,'HorizontalAlignment','center','callback','AlSortGui(''resetFeatureLimits'',''F3'');','tooltipstring','RESET plot limit of F3');
uicontrol('style','popupmenu','string','featurenames','tag','AlSortGui_listF3','units','normalized','position',[cObjPos(RowNr,1,2) cObjPos(RowNr,2,2) cObjPos(RowNr,3,2) cObjPos(RowNr,4,2)],'backgroundcolor','w','parent',hButtonPanel,'callback','AlSortGui(''updatePlots'');','tooltipstring','choose F3 from a set of features');
uicontrol('style','edit','string','','tag','AlSortGui_loLimF3','units','normalized','position',[cObjPos(RowNr,1,3) cObjPos(RowNr,2,3) cObjPos(RowNr,3,3) cObjPos(RowNr,4,3)],'backgroundcolor','w','parent',hButtonPanel,'callback','AlSortGui(''updateFeatureLimits'');','tooltipstring','set LOWER limit of F3 plot range');
uicontrol('style','edit','string','','tag','AlSortGui_hiLimF3','units','normalized','position',[cObjPos(RowNr,1,4) cObjPos(RowNr,2,4) cObjPos(RowNr,3,4) cObjPos(RowNr,4,4)],'backgroundcolor','w','parent',hButtonPanel,'callback','AlSortGui(''updateFeatureLimits'');','tooltipstring','set UPPER limit of F3 plot range');
RowNr = 4;
uicontrol('style','pushbutton','string','Time','tag','AlSortGui_resetLimTime','units','normalized','position',[cObjPos(RowNr,1,1) cObjPos(RowNr,2,1) cObjPos(RowNr,3,1) cObjPos(RowNr,4,1)],'foregroundcolor','y','backgroundcolor',pushButtonColor,'parent',hButtonPanel,'HorizontalAlignment','left','callback','AlSortGui(''resetFeatureLimits'',''Time'');','tooltipstring','RESET plot limit of time');
uicontrol('style','edit','string','','tag','AlSortGui_loLimTime','units','normalized','position',[cObjPos(RowNr,1,3) cObjPos(RowNr,2,3) cObjPos(RowNr,3,3) cObjPos(RowNr,4,3)],'backgroundcolor','w','parent',hButtonPanel,'callback','AlSortGui(''updateFeatureLimits'');','tooltipstring','set LOWER limit of time plot range');
uicontrol('style','edit','string','','tag','AlSortGui_hiLimTime','units','normalized','position',[cObjPos(RowNr,1,4) cObjPos(RowNr,2,4) cObjPos(RowNr,3,4) cObjPos(RowNr,4,4)],'backgroundcolor','w','parent',hButtonPanel,'callback','AlSortGui(''updateFeatureLimits'');','tooltipstring','set UPPER limit of time plot range');



hButtonPanel = uibuttongroup('parent',hFig,'units','centimeter','position',[dPanelPos1(5,1,1) dPanelPos1(5,2,1) dPanelPos1(5,3,1) dPanelPos1(5,4,1)], ...
    'title','Time-Windows','tag','AlSortGui_timeWindows', ...
    'foregroundcolor','y','backgroundcolor',guiColor);
cObjPos = getObjPos([1 1],4,3,[0.01 0.01],[0.01 0.01],ones(1,4).*(1/4),ones(1,3).*(1/3),100,100);
cObjPosAlt = getObjPos([1 1],2,3,[0.01 0.01],[0.01 0.01],ones(1,2).*(1/2),ones(1,3).*(1/3),100,100);
cObjPosAlt2 = getObjPos([1 1],2,4,[0.01 0.01],[0.01 0.01],ones(1,2).*(1/2),[1/3 1/3/2 1/3/2 1/3],100,100);
uicontrol('style','pushbutton','string','set','tag','AlSortGui_setTimeWindow','units','normalized','position',[cObjPosAlt(1,1,1) cObjPosAlt(1,2,1) cObjPosAlt(1,3,1) cObjPosAlt(1,4,1)],'backgroundcolor',[0.5 0.5 1.00],'parent',hButtonPanel,'callback','AlSortGui(''setTimeWindow'');');
uicontrol('style','pushbutton','string','reset','tag','AlSortGui_resetTimeWindow','units','normalized','position',[cObjPos(3,1,1) cObjPos(3,2,1) cObjPos(3,3,1) cObjPos(3,4,1)],'backgroundcolor',[.6 .6 .6],'parent',hButtonPanel,'callback','AlSortGui(''resetTimeWindow'');');
uicontrol('style','pushbutton','string','spread','tag','AlSortGui_spreadLimTime','units','normalized','position',[cObjPos(4,1,1) cObjPos(4,2,1) cObjPos(4,3,1) cObjPos(4,4,1)],'backgroundcolor',[.6 .6 .6],'parent',hButtonPanel,'callback','AlSortGui(''spreadTimeWindow'');');

uicontrol('style','text','string','move time window by','tag','AlSortGui_moveTimeWindowText','units','normalized','position',[cObjPos(1,1,2) cObjPos(1,2,2) cObjPos(1,3,2) cObjPos(1,4,2)],'foregroundcolor','m','backgroundcolor',guiColor,'parent',hButtonPanel,'HorizontalAlignment','center');
uicontrol('style','edit','string','n/a','tag','AlSortGui_moveTimeWindowStep','units','normalized','position',[cObjPos(2,1,2) cObjPos(2,2,2) cObjPos(2,3,2) cObjPos(2,4,2)],'foregroundcolor','k','backgroundcolor','w','parent',hButtonPanel,'HorizontalAlignment','center');
uicontrol('style','pushbutton','string','<','tag','AlSortGui_moveTimeWindowBackward','units','normalized','position',[cObjPosAlt2(2,1,2) cObjPosAlt2(2,2,2) cObjPosAlt2(2,3,2) cObjPosAlt2(2,4,2)],'backgroundcolor',[.6 .6 .6],'HorizontalAlignment','center','parent',hButtonPanel,'callback','AlSortGui(''moveTimeWindowBackward'');');
uicontrol('style','pushbutton','string','>','tag','AlSortGui_moveTimeWindowForward','units','normalized','position',[cObjPosAlt2(2,1,3) cObjPosAlt2(2,2,3) cObjPosAlt2(2,3,3) cObjPosAlt2(2,4,3)],'backgroundcolor',[.6 .6 .6],'HorizontalAlignment','center','parent',hButtonPanel,'callback','AlSortGui(''moveTimeWindowForward'');');
uicontrol('style','edit','string','n/a','tag','AlSortGui_TimeMarker_1','units','normalized','position',[cObjPos(1,1,3) cObjPos(1,2,3) cObjPos(1,3,3) cObjPos(1,4,3)],'foregroundcolor','k','backgroundcolor','w','parent',hButtonPanel,'HorizontalAlignment','right');
uicontrol('style','edit','string','n/a','tag','AlSortGui_TimeMarker_2','units','normalized','position',[cObjPos(2,1,3) cObjPos(2,2,3) cObjPos(2,3,3) cObjPos(2,4,3)],'foregroundcolor','k','backgroundcolor','w','parent',hButtonPanel,'HorizontalAlignment','right');
uicontrol('style','edit','string','n/a','tag','AlSortGui_TimeMarker_3','units','normalized','position',[cObjPos(3,1,3) cObjPos(3,2,3) cObjPos(3,3,3) cObjPos(3,4,3)],'foregroundcolor','k','backgroundcolor','w','parent',hButtonPanel,'HorizontalAlignment','right');
uicontrol('style','edit','string','n/a','tag','AlSortGui_TimeMarker_4','units','normalized','position',[cObjPos(4,1,3) cObjPos(4,2,3) cObjPos(4,3,3) cObjPos(4,4,3)],'foregroundcolor','k','backgroundcolor','w','parent',hButtonPanel,'HorizontalAlignment','right');


uicontrol('style','pushbutton','units','centimeter','position',[dPanelPos4(6,1,1) dPanelPos4(6,2,1) dPanelPos4(6,3,1) dPanelPos4(6,4,1)], ...
    'backgroundcolor',[0.00 0.75 0.75],'parent',hFig,'string','cut TIME','tag','AlSortGui_cutTime','callback','AlSortGui(''cutCluster'',''time'');');
uicontrol('style','pushbutton','units','centimeter','position',[dPanelPos4(6,1,2) dPanelPos4(6,2,2) dPanelPos4(6,3,2) dPanelPos4(6,4,2)], ...
    'backgroundcolor',[0.750 0.750 0.000],'parent',hFig,'string','cut FEATURE','tag','AlSortGui_cutFeature','callback','AlSortGui(''cutCluster'',''feature'');');
uicontrol('style','pushbutton','units','centimeter','position',[dPanelPos4(6,1,3) dPanelPos4(6,2,3) dPanelPos4(6,3,3) dPanelPos4(6,4,3)], ...
    'backgroundcolor',[0.750 0.375 0.000],'parent',hFig,'string','cut WAVE','tag','AlSortGui_cutWave','callback','AlSortGui(''cutCluster'',''wave'');');


end


function hFig = createFeaturePlot
hFig = figure('menubar','none','toolbar','figure','numbertitle','off','name','AlSort3: Feature vs Feature','tag','AlSortGui_FeaturePlot','color','k', ...
    'units','centimeters','position',[14 5 20 20],'resize','on','CloseRequestFcn','AlSortGui(''closeFeature'');');
axes('position',[0.05 0.05 0.9 0.9],'units','normalized','tag','AlSortGui_FeatureAxes','color','none', ...
    'xcolor',[.3 .3 .3],'ycolor',[.3 .3 .3],'zcolor',[.3 .3 .3]);

end

function hFig = createTimePlot
hFig = figure('menubar','none','toolbar','figure','numbertitle','off','name','AlSort3: Feature vs Time','tag','AlSortGui_TimePlot','color','k', ...
    'units','centimeters','position',[14 5 40 20],'resize','on','CloseRequestFcn','AlSortGui(''closeTime'');');
cObjPos1 = getObjPos([1 1],5,1,[0.01 0.05],[0.05 0.01],[0.5 0.05 0.2 0.2 0.05],[1],100,100);
cObjPos2 = getObjPos([1 1],5,3,[0.01 0.05],[0.05 0.01],[0.5 0.05 0.2 0.2 0.05],[0.9 0.05 0.05],100,100);
axes('units','normalized','position',cObjPos1(1,:,1),'tag','AlSortGui_TimeAxes','color','none', ...
    'xcolor',[.3 .3 .3],'ycolor',[.3 .3 .3],'zcolor',[.3 .3 .3]);
axes('units','normalized','position',cObjPos1(2,:,1),'tag','AlSortGui_EventAxes','color','none', ...
    'xcolor',[.3 .3 .3],'ycolor',[.3 .3 .3],'zcolor',[.3 .3 .3]);
axes('units','normalized','position',cObjPos1(3,:,1),'tag','AlSortGui_PSTHAxes','color','none', ...
    'xcolor',[.3 .3 .3],'ycolor',[.3 .3 .3],'zcolor',[.3 .3 .3]);
axes('units','normalized','position',cObjPos1(4,:,1),'tag','AlSortGui_LFPAxes','color','none', ...
    'xcolor',[.3 .3 .3],'ycolor',[.3 .3 .3],'zcolor',[.3 .3 .3]);
RowNr = 5;
uicontrol('style','slider','string','spread','tag','AlSortGui_TimeShiftSlider','callback','AlSortGui(''slideTime'');','units','normalized','position',[cObjPos2(RowNr,1,1) cObjPos2(RowNr,2,1) cObjPos2(RowNr,3,1) cObjPos2(RowNr,4,1)],'backgroundcolor',[.6 .6 .6],'parent',hFig);
uicontrol('style','pushbutton','string','zoom-IN','tag','AlSortGui_TimeZoomDown','callback','AlSortGui(''zoomInTime'');','units','normalized','position',[cObjPos2(RowNr,1,2) cObjPos2(RowNr,2,2) cObjPos2(RowNr,3,2) cObjPos2(RowNr,4,2)],'backgroundcolor',[.6 .6 .6],'parent',hFig);
uicontrol('style','pushbutton','string','zoom-OUT','tag','AlSortGui_TimeZoomUp','callback','AlSortGui(''zoomOutTime'');','units','normalized','position',[cObjPos2(RowNr,1,3) cObjPos2(RowNr,2,3) cObjPos2(RowNr,3,3) cObjPos2(RowNr,4,3)],'backgroundcolor',[.6 .6 .6],'parent',hFig);
end

function hFig = createStatPlot
hFig = figure('menubar','none','toolbar','figure','numbertitle','off','name','AlSort3: Cluster Statistics','tag','AlSortGui_StatPlot','color','k', ...
    'units','centimeters','position',[14 5 15 15],'resize','on','CloseRequestFcn','AlSortGui(''closeStat'');');
cObjPos = getObjPos([1 1],2,1,[0.01 0.01],[0.01 0.01],[0.5 0.5],[1],100,100);
axes('units','normalized','position',cObjPos(1,:,1),'tag','AlSortGui_ISIAxes','color','none', ...
    'xcolor',[.3 .3 .3],'ycolor',[.3 .3 .3],'zcolor',[.3 .3 .3]);
axes('units','normalized','position',cObjPos(2,:,1),'tag','AlSortGui_MeanWaveformAxes','color','none', ...
    'xcolor',[.3 .3 .3],'ycolor',[.3 .3 .3],'zcolor',[.3 .3 .3]);
% AlSortGui_AutoCorrAxes
% AlSortGui_SpecAxes
end

function hFig = createWaveformPlot
hFig = figure('menubar','none','toolbar','figure','numbertitle','off','name','AlSort3: Waveforms','tag','AlSortGui_WaveformPlot','color','k', ...
    'units','centimeters','position',[14 5 15 15],'resize','on','CloseRequestFcn','AlSortGui(''closeWaveform'');');
cObjPos = getObjPos([1 1],1,1,[0.01 0.01],[0.01 0.01],[1],[1],100,100);
axes('units','normalized','position',cObjPos(1,:,1),'tag','AlSortGui_WaveformAxes','color','none', ...
    'xcolor',[.3 .3 .3],'ycolor',[.3 .3 .3],'zcolor',[.3 .3 .3]);

end

function responseCode = createPushButtonsDialog(title,descriptionText,buttonStrings)
guiColor = [0 0 0];
pushButtonColor = [.3 .3 .3];
nButtons = length(buttonStrings);
hFig = dialog('menubar','none','name',title,'tag','AlSortGui_Dialog','color',guiColor, ...
    'units','normalized','position',[0.40 0.40 0.2 0.2]);
cObjPos = getObjPos([1 1],nButtons,1,[0.1 0.1],[0.05 0.05],ones(nButtons,1).*(1/nButtons),[1],100,100);
for i=1:nButtons
    uicontrol('style','pushbutton','string',buttonStrings{i},'tag','','units','normalized','position',[cObjPos(i,1,1) cObjPos(i,2,1) cObjPos(i,3,1) cObjPos(i,4,1)],'foregroundcolor','y','backgroundcolor',pushButtonColor,'parent',hFig,'callback',sprintf('set(gcbf,''userdata'',%1.0f);uiresume(gcbf);',i));
end
uiwait(hFig);
responseCode = get(hFig,'userdata');
close(hFig);
end

function responseCode = createEditDialog(title,descriptionText,editStrings,defaultStrings)
guiColor = [0 0 0];
pushButtonColor = [.3 .3 .3];
nButtons = length(editStrings);
hFig = dialog('menubar','none','name',title,'tag','AlSortGui_Dialog','color',guiColor, ...
    'units','normalized','position',[0.40 0.40 0.2 0.2]);
cCM = getPosInCM(hFig);
maxHeight = 0.75/cCM(4);
cObjPos = getObjPos([1 1],nButtons,2,[0.1 0.1],[0.05 0.05],ones(nButtons,1).*(1/nButtons),[0.3 0.7],maxHeight,NaN);
for i=1:nButtons
    uicontrol('style','text','string',editStrings{i},'tag','','units','normalized','position',[cObjPos(i,1,1) cObjPos(i,2,1) cObjPos(i,3,1) cObjPos(i,4,1)],'foregroundcolor','y','backgroundcolor',guiColor,'parent',hFig,'callback',sprintf('uiresume(gcbf);'));
    h(i) = uicontrol('style','edit','string',defaultStrings{i},'tag','','units','normalized','position',[cObjPos(i,1,2) cObjPos(i,2,2) cObjPos(i,3,2) cObjPos(i,4,2)],'foregroundcolor','k','backgroundcolor','w','parent',hFig,'callback',sprintf('uiresume(gcbf);'));
end
uiwait(hFig);
for i=1:nButtons
    responseCode{i} = get(h(i),'string');
end
close(hFig);
end


function A = setSortControl(A,option)
% option 'default'

cF = findobj('type','figure','tag','AlSortGui_SortControl');

% set listboxes and their default values
if strcmp(option,'default')
    
    % cluster
    AlSortGui('setClusterPanel',A);
    
    % Plotmode
    set(findobj('tag','AlSortGui_PlotModeAny'),'value',1);
    %             set(findobj('tag','AlSortGui_PlotModeZero'),'value',1);
    %             set(findobj('tag','AlSortGui_PlotModeOnly'),'value',1);
    
    % Sortmode
    %             set(findobj('tag','AlSortGui_SortModeExclusion'),'value',1);
    %             set(findobj('tag','AlSortGui_SortModeExclusionTW'),'value',1);
    set(findobj('tag','AlSortGui_SortModeAdd'),'value',1);
    
    % "0" color
    set(findobj('tag','AlSortGui_ZeroColorLo'),'foregroundcolor',A.ClusterColor(1,:,1));
    set(findobj('tag','AlSortGui_ZeroColorHi'),'foregroundcolor',A.ClusterColor(1,:,2));
    
    % feature list
    set(findobj('tag','AlSortGui_listF1'),'string',A.FeatureList,'value',find(strcmp(A.FeatureList,'#8')));
    set(findobj('tag','AlSortGui_listF2'),'string',A.FeatureList,'value',find(strcmp(A.FeatureList,'#14')));
    set(findobj('tag','AlSortGui_listF3'),'string',A.FeatureList,'value',find(strcmp(A.FeatureList,'#17')));
end

% set current values of other fields
AlSortGui('setFeatureLimits','Time',A.TimeLim(1,:));
AlSortGui('setFeatureLimits','F1',A.FeatureLim(1,:));
if size(A.FeatureLim,1)==1
    AlSortGui('setFeatureLimits','F2',A.FeatureLim(1,:));
    AlSortGui('setFeatureLimits','F3',A.FeatureLim(1,:));
else
    AlSortGui('setFeatureLimits','F2',A.FeatureLim(2,:));
    AlSortGui('setFeatureLimits','F3',A.FeatureLim(3,:));
end

set(findobj('tag','AlSortGui_moveTimeWindowStep'),'string','1000000');

end

function A = setTimePlot(A,option)
% setup time axes
cF = findobj('type','figure','tag','AlSortGui_TimePlot');
if isempty(cF);return;end

if strcmp(option,'default')
    set(findobj('type','uicontrol','tag','AlSortGui_TimeShiftSlider'), ...
        'SliderStep',[1000000/diff(A.TimeRange) 60000000/diff(A.TimeRange)], ...
        'Min',A.TimeRange(1),'Max',A.TimeRange(2), ...
        'Value',A.TimeLim(1)+diff(A.TimeLim)*0.5);
    
    %             set(findobj('type','uicontrol','tag','AlSortGui_TimeFeatureShiftSlider'), ...
    %                 'SliderStep',[10/diff(A.DigitalRange) 100/diff(A.DigitalRange)], ...
    %                 'Min',A.DigitalRange(1),'Max',A.DigitalRange(2), ...
    %                 'Value',A.FeatureLim(1)+diff(A.FeatureLim)*0.5);
end

if size(A.FeatureLim,1)==1
    ylim = A.FeatureLim(1,:);zlim = A.FeatureLim(1,:);
else
    ylim = A.FeatureLim(1,:);zlim = A.FeatureLim(2,:);
end
set(findobj('type','axes','tag','AlSortGui_TimeAxes'), ...
    'xlim',A.TimeLim,'ylim',ylim,'zlim',zlim);
set(findobj('type','uicontrol','tag','AlSortGui_TimeShiftSlider'), ...
    'Value',A.TimeLim(1)+diff(A.TimeLim)*0.5);

set(findobj('type','axes','tag','AlSortGui_EventAxes'), ...
    'fontsize',12,'visible','off', ...
    'xlim',A.TimeLim,'ylim',[0 2]);

set(findobj('type','axes','tag','AlSortGui_PSTHAxes'), ...
    'fontsize',12,'visible','on', ...
    'xlim',A.TimeLim);

set(findobj('type','axes','tag','AlSortGui_LFPAxes'), ...
    'fontsize',12,'visible','on', ...
    'xlim',A.TimeLim);

end

function A = setFeaturePlot(A,option)
% setup feature axes
cF = findobj('type','figure','tag','AlSortGui_FeaturePlot');
if isempty(cF);return;end
FAx = findobj('type','axes','tag','AlSortGui_FeatureAxes');
if isempty(FAx);return;end

xlim = A.FeatureLim(1,:);
if size(A.FeatureLim,1)==1
    ylim = A.FeatureLim(1,:);
    zlim = A.FeatureLim(1,:);
else
    ylim = A.FeatureLim(2,:);
    zlim = A.FeatureLim(3,:);
end

cDataAspectRatio = [1 1 1];
cDataAspectRatio = [diff(xlim) diff(ylim) diff(zlim)];

set(FAx,'dataaspectratio',cDataAspectRatio, ...
    'xlim',xlim,'xgrid','on','xcolor',[.3 .3 .3], ...
    'ylim',ylim,'ygrid','on','ycolor',[.3 .3 .3], ...
    'zlim',zlim,'zgrid','on','zcolor',[.3 .3 .3]);

% redraw axis
delete(findobj('tag','FeatureSpaceLines'));
delete(findobj('tag','FeatureSpaceText'));
line(xlim',[0;0],[0;0],'parent',FAx,'color','w','linewidth',1,'tag','FeatureSpaceLines');
line([0;0],ylim',[0;0],'parent',FAx,'color','w','linewidth',1,'tag','FeatureSpaceLines');
line([0;0],[0;0],zlim','parent',FAx,'color','w','linewidth',1,'tag','FeatureSpaceLines');
text(xlim(1),0,0,'-F1','parent',FAx,'color','w','tag','FeatureSpaceText');
text(xlim(2),0,0,'+F1','parent',FAx,'color','w','tag','FeatureSpaceText');
text(0,ylim(1),0,'-F2','parent',FAx,'color','w','tag','FeatureSpaceText');
text(0,ylim(2),0,'+F2','parent',FAx,'color','w','tag','FeatureSpaceText');
text(0,0,zlim(1),'-F3','parent',FAx,'color','w','tag','FeatureSpaceText');
text(0,0,zlim(2),'+F3','parent',FAx,'color','w','tag','FeatureSpaceText');


%             if (ischar(Option)&&strcmpi(Option,'Reset'))||(islogical(Option) && Option)
%                 xlim = A.DigitalRange;
%                 ylim = A.DigitalRange;
%                 zlim = A.DigitalRange;
%             elseif (ischar(Option)&&strcmpi(Option,'Spread'))
%                 cFeatures = AlSortGui_getAxesFeatures(A,'FeaturePlot');
%                 [XData,xlim] = AlSort_getFeature(A,cFeatures{1});
%                 [YData,ylim] = AlSort_getFeature(A,cFeatures{2});
%                 [ZData,zlim] = AlSort_getFeature(A,cFeatures{3});
%             else% get from edit boxes
%                 xlim(1) = str2double(get(findobj('tag','AlSortGui_FeaturePlotF1Lim1'),'string'));
%                 xlim(2) = str2double(get(findobj('tag','AlSortGui_FeaturePlotF1Lim2'),'string'));
%                 ylim(1) = str2double(get(findobj('tag','AlSortGui_FeaturePlotF2Lim1'),'string'));
%                 ylim(2) = str2double(get(findobj('tag','AlSortGui_FeaturePlotF2Lim2'),'string'));
%                 zlim(1) = str2double(get(findobj('tag','AlSortGui_FeaturePlotF3Lim1'),'string'));
%                 zlim(2) = str2double(get(findobj('tag','AlSortGui_FeaturePlotF3Lim2'),'string'));
%             end

end

function h = setWaveformPlot(A)
% setup Waveform axes
cF = findobj('type','figure','tag','AlSortGui_WaveformPlot');
if isempty(cF);return;end
WFAx = findobj('type','axes','tag','AlSortGui_WaveformAxes');
set(WFAx, ...
    'Gridlinestyle',':', ...
    'xlim',[0 33],'xtick',[1:32],'xgrid','on', ...
    'ylim',A.DigitalRange,'ytick',[A.DigitalRange(1) 0 A.DigitalRange(2)],'ygrid','off');
end


function h = setClusterPanel(A)
% creates the list of clusters in the cluster panel
%
% h = AlSortGui_setClusterPanel(A)
% h ... handles for all the uicontrols created

cFig = findobj('tag','AlSortGui_SortControl');
cPan = findobj('parent',cFig,'tag','AlSortGui_ClusterPanel');

set(cPan,'SelectionChangeFcn','AlSortGui(''setActiveCluster'',true);');

% check existing list
[h,ClustLabel,isActive,isSelected] = AlSortGui('getClusterPanel',A);
if ~isempty(h)
    delete(h(:));
end

if isempty(A.currentCluster);A.currentCluster = find(A.ClusterNr==0);end
emptyCluster = ~any(A.SortBuffer(:,:,1),1);
listCluster = A.ClusterNr(~emptyCluster | A.ClusterNr(:)'==A.ClusterNr(A.currentCluster));% list non-empty or active cluster
nC = length(listCluster);

% create list elements
cBackgroundColor = get(cFig,'color');
cSpace = [0.03 0.01];% hor,vert
cMargin = [0.01 0.01 0.01 0.01];% left right top bottom
cElementHeight = (1-cMargin(3)-cMargin(4)-(nC-1)*cSpace(2))/nC;
cElementWidth = [0.07 0.07 0.07 0.3 0.3];
for iC = 1:nC
    cVertPos = cMargin(4)+(iC-1)*cElementHeight+(iC-1)*cSpace(2);
    
    % Radiobutton: active cluster
    cElementNr = 1;
    cHorPos = cMargin(1);
    cPosition = [cHorPos cVertPos cElementWidth(cElementNr) cElementHeight];
    h(iC,cElementNr) = uicontrol('tag',sprintf('AlSortGui_C%1.0f_Active',listCluster(iC)),'Style','radiobutton','parent',cPan, ...
        'units','normalized','position',cPosition,'backgroundcolor',cBackgroundColor,'value',A.ClusterNr(A.currentCluster)==listCluster(iC));
    
    % tickbox: select cluster
    cElementNr = 2;
    %                 cCallback = 'AlSortGui_updatePlots(AlSortGui_retrieve(AlSort),true);';
    cCallback = '';
    cHorPos = cHorPos+cElementWidth(cElementNr-1)+cSpace(1);
    cPosition = [cHorPos cVertPos cElementWidth(cElementNr) cElementHeight];
    h(iC,cElementNr) = uicontrol('tag',sprintf('AlSortGui_C%1.0f_Select',listCluster(iC)),'Style','checkbox','parent',cPan, ...
        'units','normalized','position',cPosition,'backgroundcolor',cBackgroundColor, ...
        'callback',cCallback);
    if any(ClustLabel(isSelected)==listCluster(iC))
        set(h(iC,cElementNr),'value',1);
    end
    
    % text: cluster label
    cElementNr = 3;
    if A.ClusterNr(A.currentCluster)==listCluster(iC);cColorIndex = 2;else cColorIndex = 1;end
    cHorPos = cHorPos+cElementWidth(cElementNr-1)+cSpace(1);
    cPosition = [cHorPos cVertPos cElementWidth(cElementNr) cElementHeight];
    h(iC,cElementNr) = uicontrol('tag',sprintf('AlSortGui_C%1.0f_Label',listCluster(iC)),'Style','text','parent',cPan, ...
        'units','normalized','position',cPosition,'backgroundcolor',cBackgroundColor, ...
        'string',sprintf('%1.0f',listCluster(iC)),'fontweight','normal','foregroundcolor',A.ClusterColor(listCluster(iC)+1,:,cColorIndex));
    
    % text: total spike number
    cElementNr = 4;
    nClust = sum(A.SortBuffer(:,A.ClusterNr==listCluster(iC),1));
    nTotal = size(A.SortBuffer,1);
    cHorPos = cHorPos+cElementWidth(cElementNr-1)+cSpace(1);
    cPosition = [cHorPos cVertPos cElementWidth(cElementNr) cElementHeight];
    h(iC,cElementNr) = uicontrol('tag',sprintf('AlSortGui_C%1.0f_FirstTime',listCluster(iC)),'Style','text','parent',cPan, ...
        'units','normalized','position',cPosition,'backgroundcolor',cBackgroundColor,'horizontalalignment','right', ...
        'string',sprintf('%1.0f %3.0f%%',nClust,nClust/nTotal*100),'foregroundcolor',[.8 .8 .8],'fontsize',8);
    
    % text: window spike number
    cElementNr = 5;
    nClust = sum(A.SortBuffer(:,A.ClusterNr==listCluster(iC),1) & A.Data.TimeStamps>=A.TimeWin(1) & A.Data.TimeStamps<=A.TimeWin(2));
    nTotal = sum(A.Data.TimeStamps>=A.TimeWin(1) & A.Data.TimeStamps<=A.TimeWin(2));
    cHorPos = cHorPos+cElementWidth(cElementNr-1)+cSpace(1);
    cPosition = [cHorPos cVertPos cElementWidth(cElementNr) cElementHeight];
    h(iC,cElementNr) = uicontrol('tag',sprintf('AlSortGui_C%1.0f_LastTime',listCluster(iC)),'Style','text','parent',cPan, ...
        'units','normalized','position',cPosition,'backgroundcolor',cBackgroundColor,'horizontalalignment','right', ...
        'string',sprintf('%1.0f %3.0f%%',nClust,nClust/nTotal*100),'foregroundcolor',[.8 .8 .8],'fontsize',8);
end

% adjust panel/figure sizeFFIG_LFPTrace
cUnits = 'points';
oldFigUnits = get(cFig,'units');
set(cFig,'units',cUnits);
cFigSize = get(cFig,'position');

oldPanUnits = get(cPan,'units');
set(cPan,'units',cUnits);
cPanSize = get(cPan,'position');

PointsPerCluster = 17;
cPointSizeNeeded = max([30 nC*PointsPerCluster]);
dPoints = cPointSizeNeeded -cPanSize(4);

cFigSize(2) = cFigSize(2)-dPoints;
cFigSize(4) = cFigSize(4)+dPoints;
cPanSize(4) = cPanSize(4)+dPoints;

set(cFig,'position',cFigSize);
set(cPan,'position',cPanSize);

set(cFig,'units',oldFigUnits);
set(cPan,'units',oldPanUnits);
end



function [h,ClustLabel,isActive,isSelected] = getClusterPanel(A)
% read the existing lists of clusters
%
% [h,ClustLabel,isActive,isSelected] = AlSortGui_getClusterPanel(A)
% h ............ handles of the uicontrols
% CLustlabel ... cell array cluster names '0','1', ...
% isActive ..... the clusterlabel marked by the radiobutton
% isSelected ... true for ticked tickboxes

h = [];ClustLabel = [];isActive = [];isSelected = logical(0);
cFig = findobj('tag','AlSortGui_SortControl');
cPan = findobj('parent',cFig,'tag','AlSortGui_ClusterPanel');
cPanKids = findobj('parent',cPan);
nKids = length(cPanKids(:))-1;
cC = -1;cCNr = 0;
while nKids>0 && numel(h)<nKids
    cC = cC+1;
    cH = findobj('parent',cPan,'tag',sprintf('AlSortGui_C%1.0f_Active',cC));
    if ~isempty(cH)
        cCNr = cCNr+1;
        h(cCNr,1) = cH;
        if get(h(cCNr,1),'value')==1;isActive = cC;end
        h(cCNr,2) = findobj('parent',cPan,'tag',sprintf('AlSortGui_C%1.0f_Select',cC));
        if get(h(cCNr,2),'value')==1;isSelected(cCNr) = true;else isSelected(cCNr) = false;end
        h(cCNr,3) = findobj('parent',cPan,'tag',sprintf('AlSortGui_C%1.0f_Label',cC));
        h(cCNr,4) = findobj('parent',cPan,'tag',sprintf('AlSortGui_C%1.0f_FirstTime',cC));
        h(cCNr,5) = findobj('parent',cPan,'tag',sprintf('AlSortGui_C%1.0f_LastTime',cC));
        ClustLabel(cCNr) = cC;
    end
end
end



function updatePlots(A,UsePlotMode)

if ~isempty(A.TimeWin)
    i = 1;
    cLowBoundTextbox = findobj('tag',sprintf('AlSortGui_TimeMarker_%1.0f',i));
    set(cLowBoundTextbox,'string',sprintf('%1.0f',A.TimeWin(i)));
    i = 2;
    cHighBoundTextbox = findobj('tag',sprintf('AlSortGui_TimeMarker_%1.0f',i));
    set(cHighBoundTextbox,'string',sprintf('%1.0f',A.TimeWin(i)));
end


if UsePlotMode % update plots according to PlotMode
    if get(findobj('tag','AlSortGui_PlotModeOnly'),'value') == 1
        ClusterToPlot = A.ClusterNr(A.currentCluster);
    elseif get(findobj('tag','AlSortGui_PlotModeZero'),'value') == 1
        ClusterToPlot = [0 A.ClusterNr(A.currentCluster)];
    elseif get(findobj('tag','AlSortGui_PlotModeAny'),'value') == 1
        [h,ClustLabel,isActive,isSelected] = AlSortGui('getClusterPanel',A);
        MarkedClusterNr = ClustLabel(isSelected);
        ClusterToPlot = [MarkedClusterNr(:)' A.ClusterNr(A.currentCluster)];
    end
    HideOther = true;
else  % update only current cluster
    ClusterToPlot = A.ClusterNr(A.currentCluster);
    HideOther = false;
end

ClusterToPlot = unique(ClusterToPlot);

% plot to time axes
TimeWindowTextbox = findobj('tag','AlSortGui_TimePlotTimeWindows');
if ~isempty(TimeWindowTextbox)
    set(TimeWindowTextbox,'string',num2str(round(sort(A.TimeWin(:)).*10)./10));
end

TargetAx = findobj('type','axes','tag','AlSortGui_TimeAxes');
if ~isempty(TargetAx)
    cFeatures = AlSortGui('getFeatures');
    cFeatures = {'Time',cFeatures{1},cFeatures{2}};
    AlSortGui('plotClusterData',A,ClusterToPlot,cFeatures,TargetAx,HideOther,false);
    A = AlSortGui('plotTimeWindow',A,TargetAx);
end

% plot to PSTH axes
TargetAx = findobj('type','axes','tag','AlSortGui_PSTHAxes');
if ~isempty(TargetAx)
    plotPSTH(A,ClusterToPlot,TargetAx);
end

% plot to Events axes
TargetAx = findobj('type','axes','tag','AlSortGui_EventAxes');
if ~isempty(TargetAx)
    AlSortGui('plotEvents',A);
end

% plot to LFP axes
TargetAx = findobj('type','axes','tag','AlSortGui_LFPAxes');
if ~isempty(TargetAx)
    AlSortGui('plotAnalog',A,0,TargetAx);
end

% plot to feature axes
TargetAx = findobj('type','axes','tag','AlSortGui_FeatureAxes');
if ~isempty(TargetAx)
    cFeatures = AlSortGui('getFeatures');
    AlSortGui('plotClusterData',A,ClusterToPlot,cFeatures,TargetAx,HideOther,true);
end

% plot to Waveform axes
TargetAx = findobj('type','axes','tag','AlSortGui_WaveformAxes');
if ~isempty(TargetAx)
    plotWaveforms(A,ClusterToPlot,TargetAx,true,false);
end

% plot to ISI axes
TargetAx = findobj('type','axes','tag','AlSortGui_ISIAxes');
if ~isempty(TargetAx)
    plotISI(A,ClusterToPlot,TargetAx,true);
end

% plot to mean Waveform axes
TargetAx = findobj('type','axes','tag','AlSortGui_MeanWaveformAxes');
if ~isempty(TargetAx)
    plotWaveforms(A,ClusterToPlot,TargetAx,true,true);
end

% plot to Spectrum
TargetAx = findobj('type','axes','tag','AlSortGui_SpecAxes');
if ~isempty(TargetAx)
    %                 AlSortGui_plotSpec(A,ClusterToPlot,TargetAx,true);
end
end

function plotWaveforms(A,ClusterToPlot,TargetAx,UseTimeWin,MeanFlag)

if nargin<5
    MeanFlag = true;
end

% check time window
if UseTimeWin && ~isempty(A.TimeWin)
    InTime = A.Data.TimeStamps>=A.TimeWin(1) & A.Data.TimeStamps<=A.TimeWin(2);
else
    InTime = true(size(A.Data.TimeStamps));
end

% order of plotting
iscurrCluster = ClusterToPlot==A.ClusterNr(A.currentCluster);
isZeroCluster = ClusterToPlot==0;
ClusterToPlot = [ClusterToPlot(isZeroCluster) ClusterToPlot(~iscurrCluster&~isZeroCluster) ClusterToPlot(iscurrCluster)];

% restrict number of waveforms to plot
[isClust,iClust] = ismember(ClusterToPlot,A.ClusterNr);
iiWF = InTime & any(A.SortBuffer(:,iClust,1),2);
iWF = find(iiWF);
nWF = length(iWF);
if nWF>A.nWaveform && ~MeanFlag
    iWFsub = randsample(nWF,A.nWaveform);
    iWF = iWF(iWFsub);
    nWF = A.nWaveform;
    iiWF(:) = false;
    iiWF(iWF) = true;
end

cla(TargetAx)
for iPlot = 1:length(ClusterToPlot)
    iClust = A.ClusterNr == ClusterToPlot(iPlot);
    if ~any(iClust);continue;end
    if find(iClust)==A.currentCluster
        ColorIndex = 2;
    else
        ColorIndex = 1;
    end
    
    % find existing clusterplot
    cTag = sprintf('Cluster#%1.0f',A.ClusterNr(iClust));
    %                 cHandle = findobj('parent',TargetAx,'tag',cTag);
    
    cN = sum(A.SortBuffer(:,iClust,1)&iiWF);
    
    if MeanFlag
        [WFm,WFs,WFse] = agmean(A.Data.SpikeWaveForm(:,1,A.SortBuffer(:,iClust,1)&iiWF),[],3);
        line([1:32]',WFm(:),'tag',cTag,'parent',TargetAx,'linestyle','-','linewidth',2.0,'color',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex));

        % plot std
        line([1:32]',WFm(:)+WFs(:),'tag',cTag,'parent',TargetAx,'linestyle',':','linewidth',0.75,'color',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex));
        line([1:32]',WFm(:)-WFs(:),'tag',cTag,'parent',TargetAx,'linestyle',':','linewidth',0.75,'color',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex));

        % plot range
        hull = NLX_getBoundaries(A.Data,A.SortBuffer(:,iClust,1)&iiWF);
        line([1:32;1:32]',hull,'tag',cTag,'parent',TargetAx,'linestyle','--','linewidth',0.75,'color',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex));
    else
        line(repmat([1:32]',1,cN),permute(A.Data.SpikeWaveForm(:,1,A.SortBuffer(:,iClust,1)&iiWF),[1 3 2]), ...
            'tag',cTag,'parent',TargetAx, ...
            'linewidth',0.75,'color',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex));
    end
end
end

function currClusterPlot = plotClusterData(A,ClusterToPlot,FeatureName,TargetAx,HideOtherCluster,UseTimeWin)

if UseTimeWin && ~isempty(A.TimeWin)
    InTime = A.Data.TimeStamps>=A.TimeWin(1) & A.Data.TimeStamps<=A.TimeWin(2);
else
    InTime = true(size(A.Data.TimeStamps));
end

for iPlot = 1:length(ClusterToPlot)
    iClust = A.ClusterNr == ClusterToPlot(iPlot);
    currClusterPlot(iPlot) = NaN;
    if ~any(iClust);continue;end
    
    if ClusterToPlot(iPlot)==0
        if get(findobj('tag','AlSortGui_ZeroColorAuto'),'value')==1 && get(findobj('tag','AlSortGui_SortModeAdd'),'value')==1
            ColorIndex = 2;
        elseif get(findobj('tag','AlSortGui_ZeroColorHi'),'value')==1
            ColorIndex = 2;
        else
            ColorIndex = 1;
        end
    elseif find(iClust)==A.currentCluster
        ColorIndex = 2;
    else
        ColorIndex = 1;
    end
    
    TSIndex = InTime & A.SortBuffer(:,iClust,1);
    if ~any(TSIndex);continue;end
    XYZData = zeros(sum(TSIndex),3);
    
    for i=1:length(FeatureName)
        XYZData(:,i) = AlSort_getFeature(A,FeatureName{i},TSIndex);
    end
    
    % find existing clusterplot
    cTag = sprintf('Cluster#%1.0f',A.ClusterNr(iClust));
    cHandle = findobj('parent',TargetAx,'tag',cTag);
    
    % create cluster plot
    if isempty(cHandle);cHandle = line(NaN,NaN,TargetAx,'tag',cTag,'parent',TargetAx);end
    
    % update cluster plot
    set(cHandle,'xdata',XYZData(:,1),'ydata',XYZData(:,2),'zdata',XYZData(:,3), ...
        'color',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex),'markeredgecolor',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex),'markerfacecolor',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex),A.ClusterProp{:});
    %                 set(cHandle,'xdata',XYZData(:,1),'ydata',XYZData(:,2),'zdata',[], ...
    %                     'color',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex),'markeredgecolor',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex),'markerfacecolor',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex),A.ClusterProp{:});
    
    currClusterPlot(iPlot) = cHandle;
end

% hide other cluster
if HideOtherCluster
    h = get(TargetAx,'children');
    for i=1:length(h)
        if ~isempty(findstr('Cluster#',get(h(i),'tag'))) && ~any(currClusterPlot==h(i))
            set(h(i), 'xdata', NaN,'ydata', NaN,'zdata', NaN);
        end
    end
end
end


function A = plotTimeWindow(A,FigH)
n = length(A.TimeWin);
x = [A.TimeWin;A.TimeWin];
y = repmat(A.DigitalRange',1,n);
h = findobj('parent',FigH,'tag','TimeWin');
if ~isempty(h);delete(h(:));end
h = line(x,y,'tag','TimeWin','parent',FigH,'clipping','off',A.TimeWinProp{:});
end


% ------------------------------------------------------------------------
% ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
% ------------------------------------------------------------------------

function cObjPos = getObjPos(sizeArea,m,n,objMargin,objSpacing,mRatios,nRatios,maxHeight,maxWidth)
% cPosInCm .... field size
% m ........... rows
% n ........... columns
% cObjPos ..... [m x 4 x n] normalized positions;
cObjPos = zeros(m,4,n);
objHeight = (sizeArea(1) - 2*objMargin(1) - (m-1)*objSpacing(1)) .* mRatios;
objHeight(objHeight>maxHeight) = maxHeight;
objWidth = (sizeArea(2) - 2*objMargin(2) - (n-1)*objSpacing(2)) .* nRatios;
objWidth(objWidth>maxWidth) = maxWidth;
for i=1:m
    for j=1:n
        if j>1
            cObjPos(i,1,j) = objMargin(2)+sum(objWidth(1:j-1))+(j-1)*objSpacing(2);
        else
            cObjPos(i,1,j) = objMargin(2);
        end
        cObjPos(i,2,j) = sizeArea(1)-objMargin(1)-sum(objHeight(1:i))-(i-1)*objSpacing(1);
        cObjPos(i,3,j) = objWidth(j);
        cObjPos(i,4,j) = objHeight(i);
    end
end
end

function currPosition = getPosInCM(h)
oldUnits = get(h,'units');
set(h,'units','centimeter');
currPosition = get(h,'position');
set(h,'units',oldUnits);
end

function A = cutCluster(A,XYZFeature,PolygonNodes,UseTimeWin,SpaceRotation)
% number of XYZFeature determines Polygon or Boundaries
%
% PolygonNodes ... currentpoint property of axes

if nargin<5;SpaceRotation = [];end
if A.ClusterNr(A.currentCluster)==0;return;end

% in feature selection ------------------------------
isIN = AlSort_inFeaturePolygon(A,XYZFeature,PolygonNodes,SpaceRotation);
InverseFlag = get(findobj('tag','AlSortGui_SortInverseSelection'),'value')==1;
TimeWinFlag = get(findobj('tag','AlSortGui_SortModeExclusionTW'),'value')==1;

% in time window ------------------------------
% if (UseTimeWin && ~isempty(A.TimeWin)) || TimeWinFlag
if TimeWinFlag && ~isempty(A.TimeWin)
    inTime = NLX_findSpikes(A.Data,'TIME',A.TimeWin([1 2]));
else
    inTime = true(size(A.Data.TimeStamps));
end

TakeFromZero = true;
A = AlSort_SortBuffer(A,'NEW');
isClust = A.SortBuffer(:,A.currentCluster,1);
OtherCluster = A.ClusterNr~=0 & A.ClusterNr~=A.ClusterNr(A.currentCluster);
isFree = ~any(A.SortBuffer(:,OtherCluster,1),2);


% if get(findobj('tag','AlSortGui_SortModeExclusion'),'value')==1
%     if ~InverseFlag
%         A.SortBuffer(isIN & isClust,A.currentCluster,1) = true;% keep insiders (redundant)
%         A.SortBuffer(~isIN & isClust,A.currentCluster,1) = false;% delete outsiders
%         A.SortBuffer(~isIN & isClust,A.ClusterNr==0,1) = true;% outsiders to #0
%     else
%         A.SortBuffer(isIN & isClust,A.currentCluster,1) = false;% exclude insiders
%         A.SortBuffer(isIN & isClust,A.ClusterNr==0,1) = true;% outsiders to #0
%     end
    
if get(findobj('tag','AlSortGui_SortModeExclusion'),'value')==1
    if ~InverseFlag
        % add outside time window
        % exclude inside time window
        A.SortBuffer(inTime & isIN & isClust,A.currentCluster,1) = true;% keep insiders (redundant)
        A.SortBuffer(inTime & ~isIN & isClust,A.currentCluster,1) = false;% delete outsiders
        A.SortBuffer(inTime & ~isIN & isClust,A.ClusterNr==0,1) = true;%add to zero cluster
    else
        A.SortBuffer(inTime & isIN & isClust,A.currentCluster,1) = false;% exclude insiders
        A.SortBuffer(inTime & isIN & isClust,A.ClusterNr==0,1) = true;%add to zero cluster
    end
    
elseif get(findobj('tag','AlSortGui_SortModeAdd'),'value')==1
    ButtonName = questdlg(sprintf('Are you sure you want to add selection to cluster #%1.0f ?',A.ClusterNr(A.currentCluster)), 'cluster cutting', 'No');
    if strcmpi(ButtonName,'Yes')
        A.SortBuffer(isIN & isFree & inTime,A.currentCluster,1) = true;
        A.SortBuffer(isIN & isFree & inTime,A.ClusterNr==0,1) = false;
    end
    
elseif get(findobj('tag','AlSortGui_SortModeSplit'),'value')==1
    if ~InverseFlag
        A.SortBuffer(inTime & isIN & isClust,A.currentCluster,1) = true;% keep insiders (redundant)
        A.SortBuffer(inTime & ~isIN & isClust,A.currentCluster,1) = false;% delete outsiders
        cCluster = A.currentCluster;% keep current cluster in mind
        A = AlSort_SortBuffer(A,'ADDCLUST');% add a new one to dump the excluded ones in
        A.SortBuffer(inTime & ~isIN & isClust,A.currentCluster,1) = true;
        A.currentCluster = cCluster;% switch back to original current cluster
    else
        A.SortBuffer(inTime & isIN & isClust,A.currentCluster,1) = false;% keep insiders (redundant)
        cCluster = A.currentCluster;% keep current cluster in mind
        A = AlSort_SortBuffer(A,'ADDCLUST');% add a new one to dump the excluded ones in
        A.SortBuffer(inTime & isIN & isClust,A.currentCluster,1) = true;
        A.currentCluster = cCluster;% switch back to original current cluster
    end
else
    error('Don''t know which Sort-Mode we''re in ?!');
end

A = AlSort_SortBuffer(A,'CLEARCLUST');
end

function Pt = selectPolygon(A)
% Pt ... [2 x 3 x n]
doSelect = true;
xv = NaN;yv = NaN;zv = NaN;Pt = [];
SelLine = line(xv,yv,'linestyle','none','tag','SelLine', ...
    'marker','o','markersize',3,'markerfacecolor','m','markeredgecolor','none','clipping','off');
SelPolygon = patch('xdata',xv,'ydata',yv,'zdata',zv,'facecolor','none','edgecolor','m','linewidth',1,'clipping','off');
xv = [];yv = [];zv = [];SelCnt = 0;
while doSelect
    [xi,yi,button,cPt] = ginput(1);
    if button==1
        SelCnt = SelCnt+1;
        xv = cat(1,xv,cPt(1,1));
        yv = cat(1,yv,cPt(1,2));
        zv = cat(1,zv,cPt(1,3));
        Pt = cat(3,Pt,cPt);
        set(SelLine,'xdata',xv,'ydata',yv,'zdata',zv);
        if SelCnt>1
            set(SelPolygon,'xdata',xv,'ydata',yv,'zdata',zv);
        end
    else
        doSelect = false;
    end
end
delete([SelPolygon,SelLine]);
end

function yv = selectWaveform(A)
doSelect = true;
yv = repmat(A.DigitalRange(:),[1 32]);
xv = [1:32;1:32];
Pt = [];
SelLine = line(xv,yv,'linestyle','-','color',[0.6 0.6 0.6],'tag','SelLine', ...
    'marker','o','markersize',3,'markerfacecolor','m','markeredgecolor','none','clipping','off');
while doSelect
    [xi,yi,button,cPt] = ginput(1);
    if button==1
        xi = round(xi);
        yv(1,xi) = yi;
        yv(2,xi) = NaN;
        Pt = cat(3,Pt,cPt);
        set(SelLine(xi),'ydata',yv(:,xi));
        
        [xiDummy,yi,button,cPt] = ginput(1);
        if yi>yv(1,xi)
            yv(1,xi) = yv(1,xi);
            yv(2,xi) = yi;
        else
            yv(2,xi) = yv(1,xi);
            yv(1,xi) = yi;
        end
        set(SelLine(xi),'ydata',yv(:,xi));
        
    else
        doSelect = false;
    end
end
delete(SelLine);
end

function cFeatures = getAxesFeatures(A,FigureOption)
switch FigureOption
    case 'TimePlot'
        TAx = findobj('type','axes','tag','AlSortGui_TimeAxes');
        if ~isempty(TAx)
            cFeatures{1} = 'Time';
            for i=1:3
                FeatList = get(findobj('type','uicontrol','tag',sprintf('AlSortGui_listF%1.0f',i)),'string');
                FeatNr = get(findobj('type','uicontrol','tag',sprintf('AlSortGui_listF%1.0f',i)),'value');
                cFeatures{i+1} = FeatList{FeatNr};
                if strcmp(FeatList{FeatNr}(1),'#')
                    cFeatures{i+1} = str2double(FeatList{FeatNr}(2:end));
                else
                    cFeatures{i+1} = FeatList{FeatNr};
                end
            end
            
        end
    case 'FeaturePlot'
        FAx = findobj('type','axes','tag','AlSortGui_FeatureAxes');
        if ~isempty(FAx)
            for i=1:3
                FeatList = get(findobj('type','uicontrol','tag',sprintf('AlSortGui_listF%1.0f',i)),'string');
                FeatNr = get(findobj('type','uicontrol','tag',sprintf('AlSortGui_listF%1.0f',i)),'value');
                cFeatures{i} = FeatList{FeatNr};
                if strcmp(FeatList{FeatNr}(1),'#')
                    cFeatures{i} = str2double(FeatList{FeatNr}(2:end));
                else
                    cFeatures{i} = FeatList{FeatNr};
                end
            end
        end
end

end

function [filename,pathname] = getFilePath(A,defaultExt,dialogTitle,param)
currDir = cd;
% get default directory from spike data
if isempty(A.Data)
    A.Data.Path = '';
    defaultDir = currDir;
elseif ~isempty(A.Data)&&~isempty(A.Data.Path)%
    [fDir,fName,fExt] = fileparts(A.Data.Path);
    defaultDir = fDir;
else
    defaultDir = currDir;
end
cd(defaultDir);
[filename,pathname] = uigetfile(defaultExt,dialogTitle,param{:});
cd(currDir);
if pathname==0;return;end
end

function [userh,digh] = plotEvents(A)
userh = [];
digh = [];
cAx = findobj('tag','AlSortGui_EventAxes');
if isempty(cAx) || isempty(A.Events);return;end
userNEV = NLX_getEventType(A.Events,'user');
digitalNEV = NLX_getEventType(A.Events,'digital');
cla(cAx);

userh = line([userNEV.TimeStamps(:)';userNEV.TimeStamps(:)'], ...
    [zeros(1,length(userNEV.TimeStamps));ones(1,length(userNEV.TimeStamps))], ...
    'parent',cAx,'linewidth',0.75,'color','magenta');
%NEV.Eventstring

%             digh = line([digitalNEV.TimeStamps(:)';digitalNEV.TimeStamps(:)'], ...
%                 [zeros(1,length(digitalNEV.TimeStamps));ones(1,length(digitalNEV.TimeStamps))]+1, ...
%                 'parent',cAx,'linewidth',0.75,'color','cyan');
digh = line([digitalNEV.TimeStamps(:)'], ...
    [ones(1,length(digitalNEV.TimeStamps))].*1.5, ...
    'parent',cAx,'linewidth',0.75,'color','cyan','linestyle','none', ...
    'marker','o','markeredgecolor','none','markerfacecolor','cyan','markersize',2);
%NEV.TTL

set(cAx,'visible','off');
end

function plotAnalog(A,i,TargetAx)
if isempty(A.DataAnalog);return;end
InTime = A.DataAnalog.TimeStamps>=A.TimeLim(1) & A.DataAnalog.TimeStamps<=A.TimeLim(2);
cla(TargetAx);
line(A.DataAnalog.TimeStamps(InTime),A.DataAnalog.Samples(InTime),'color','y','parent',TargetAx);
[mn,mx] = GetDataRange(TargetAx);
ylim = [min([min(mn(:,2,:)) 0]) max([max(mx(:,2,:))*1.0 1])];
if isnan(ylim(2));ylim(2)=1;end
set(TargetAx,'fontsize',10,'tickdir','out','ylim',ylim,'ytick',ylim(2),'yticklabel',ylim(2));
end

function setMarkedClusters(A,ClusterNr)
% set cluster tickboxes
[h,ClustLabel,isActive,isSelected] = AlSortGui('getClusterPanel',A);
for i = 1:length(ClustLabel)
    if any(ClusterNr==ClustLabel(i))
        set(findobj('tag',sprintf('AlSortGui_C%1.0f_Select',ClustLabel(i))),'value',1);
    else
        set(findobj('tag',sprintf('AlSortGui_C%1.0f_Select',ClustLabel(i))),'value',0);
    end
end
end

function A = saveFile(A,option)
% Saves a NSE data file
%
% A = AlSortGui_SaveFile(A,option)
% option ... 'normal' saves data as is
%            'special' saves selected cluster and time windows separately

if nargin<2;AllClusterFlag=true;end
FileSuffix = {};
if ~AlSort_CheckClustering(A);
    return;
end

% rewrite cluster information in data
for i=1:length(A.ClusterNr)
    A.Data.ClusterNr(A.SortBuffer(:,i,1)) = A.ClusterNr(i);
end

% select specific clusters
if strcmp(option,'special')
    % select clusters
    ButtonName = questdlg('Which clusters do you want to save ?','save to *.Nse','all', 'selected', 'all');
    switch ButtonName
        case 'selected'
            [h,ClustLabel,isActive,isSelected] = AlSortGui('getClusterPanel',A);
            cClusterIndex = any(A.SortBuffer(:,ismember(A.ClusterNr,ClustLabel(isSelected)),1),2);
            A.Data = NLX_ExtractNSE(A.Data,cClusterIndex);
    end
end

% create multiple data files
TimeSplitOption = 'none';
if length(A.FileNames) > 1
    TimeSplitOption = questdlg('Do you want to save merged files separately ? (if you want to split this file into custom time windows, save it as a whole first and then choose "save special") ','save to *.Nse','No', 'demerge', 'No');
elseif strcmp(option,'special') && length(A.FileNames)<=1
    TimeSplitOption = questdlg('Choose option to split the data ...','save to *.Nse', ...
        'none', 'window', 'split', ...
        'none');
end

switch TimeSplitOption
    case 'window'
        [A.Data,Index] = NLX_timesplitNSE(A.Data,[A.TimeWin(1) A.TimeWin(2)]);
        FileSuffix = {sprintf('1_%1.0f_%1.0f',A.TimeWin(1),A.TimeWin(2))};
    case 'split'
        [A.Data,Index] = NLX_timesplitNSE(A.Data,A.TimeWin(:));
        cTimeBounds = [[A.TimeRange(1);A.TimeWin(:)] [A.TimeWin(:);A.TimeRange(2)]];
        for i=1:length(A.Data)
            FileSuffix = sprintf('%1.0f_%1.0f_%1.0f',i,cTimeBounds(i,1),cTimeBounds(i,2));
            [cSaveDir,cSaveFileName,cSaveExt] = fileparts(A.Data{i}.Path);
            A.Data{i}.Path = fullfile(cSaveDir,[cSaveFileName,'.' FileSuffix,cSaveExt]);
        end
    case 'demerge'
        [A.Data,Index] = NLX_timesplitNSE(A.Data,A.FileTimes);
        for i=1:length(A.Data)
            A.Data{i}.Path = fullfile(A.WorkDir,A.FileNames{i});
        end
end

% ask for filename and save
if isstruct(A.Data)
    [filename,pathname] = uiputfile(A.Data.Path,'write to *.NSE file');
    if filename==0;return;end
    A.Data.Path = fullfile(pathname,filename);
    NLX_SaveNSE(A.Data,false,false);
elseif iscell(A.Data)
    for i=1:length(A.Data)
        [filename,pathname] = uiputfile(A.Data{i}.Path,'write to *.NSE file');
        if filename==0;continue;end
        A.Data{i}.Path = fullfile(pathname,filename);
        NLX_SaveNSE(A.Data{i},false,false);
    end
end
end

function A = removePath(A)
if isunix
    currMatlabPath = textscan(path,'%s','delimiter',':');
else
    currMatlabPath = textscan(path,'%s','delimiter',';');
end
currMatlabPath = currMatlabPath{1};
currProgPath = which('AlSort');
cPPDir = fileparts(currProgPath);
cIndex = strmatch({cPPDir},currMatlabPath);
cPathLengths = cellfun('size',currMatlabPath(cIndex),2);
cIndex(cPathLengths==length(cPPDir))=[];
fprintf('Remove from Matlab-Path:\n');
fprintf('%s\n',currMatlabPath{cIndex});
rmpath(currMatlabPath{cIndex});
end

function plotISI(A,ClusterToPlot,TargetAx,UseTimeWin)
% check time window
if UseTimeWin && ~isempty(A.TimeWin)
    InTime = A.Data.TimeStamps>=A.TimeWin(1) & A.Data.TimeStamps<=A.TimeWin(2);
else
    InTime = true(size(A.Data.TimeStamps));
end

% order of plotting
iscurrCluster = ClusterToPlot==A.ClusterNr(A.currentCluster);
isZeroCluster = ClusterToPlot==0;
ClusterToPlot = [ClusterToPlot(isZeroCluster) ClusterToPlot(~iscurrCluster&~isZeroCluster) ClusterToPlot(iscurrCluster)];

cla(TargetAx)
for iPlot = 1:length(ClusterToPlot)
    iClust = A.ClusterNr == ClusterToPlot(iPlot);
    if find(iClust)==A.currentCluster
        ColorIndex = 2;
    else
        ColorIndex = 1;
    end
    
    % find existing clusterplot
    cTag = sprintf('Cluster#%1.0f',A.ClusterNr(iClust));
    
    cN = sum(A.SortBuffer(:,iClust,1) & InTime);
    
    ISIs = diff(A.Data.TimeStamps(A.SortBuffer(:,iClust,1) & InTime))./(10^3);
    
    tLim = log10([0.0 5000]);
    tTick = log10([1 10 100 1000]);
    tTickLabel = {'1' '10' '100' '1000'};
    %                 HistBinEdges = linspace(log10(0.5),log10(5000),50);
    %                 [HistN,HistBIN] = histc(log10(ISIs),HistBinEdges);
    %                 axes(TargetAx);hold on;
    HistBinCentres = linspace(log10(0.5),log10(5000),50);
    [HistN,HistBIN] = hist(log10(ISIs),HistBinCentres);
    h = PlotHistData(HistBinCentres',HistN','LINEBAR', ...
        'parent',TargetAx, ...
        'color',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex));
    
    [mn,mx] = GetDataRange(TargetAx);
    ylimScale = 1.0;
    ylim = [0 max([1 max(mx(:,2,:)) * ylimScale])];
    if isnan(ylim(2));ylim(2)=1;end
    set(TargetAx,'fontsize',6,'tickdir','out', ...
        'xlim',tLim,'xtick',tTick,'xticklabel',tTickLabel, ...
        'ylim',ylim,'ytick',ylim(2),'yticklabel',ylim(2));
end

end

function plotSpec(A,ClusterToPlot,TargetAx,UseTimeWin)
% check time window
if UseTimeWin && ~isempty(A.TimeWin)
    InTime = A.Data.TimeStamps>=A.TimeWin(1) & A.Data.TimeStamps<=A.TimeWin(2);
else
    InTime = true(size(A.Data.TimeStamps));
end

% order of plotting
iscurrCluster = ClusterToPlot==A.ClusterNr(A.currentCluster);
isZeroCluster = ClusterToPlot==0;
ClusterToPlot = [ClusterToPlot(isZeroCluster) ClusterToPlot(~iscurrCluster&~isZeroCluster) ClusterToPlot(iscurrCluster)];

cla(TargetAx)
for iPlot = 1:length(ClusterToPlot)
    iClust = A.ClusterNr == ClusterToPlot(iPlot);
    if find(iClust)==A.currentCluster
        ColorIndex = 2;
    else
        ColorIndex = 1;
    end
    
    % find existing clusterplot
    cTag = sprintf('Cluster#%1.0f',A.ClusterNr(iClust));
    cN = sum(A.SortBuffer(:,iClust,1) & InTime);
    
    data = A.Data.TimeStamps(A.SortBuffer(:,iClust,1) & InTime)./(10^6);
    params.tapers = [3 5];
    params.pad = 1;
    params.Fs = 32000;
    params.fpass = [0 150];
    params.trialave = 0;
    [S,f]=mtspectrumpt(data(1:min([cN 100])),params);%,fscorr,t);
    
    line(f(:),S(:), ...
        'parent',TargetAx,'color',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex));
    
    [mn,mx] = GetDataRange(TargetAx);
    ylim = [0 max(mx(:,2,:))*1.0];
    if isnan(ylim(2));ylim(2)=1;end
    set(TargetAx,'fontsize',6,'tickdir','out', ...
        'xlim',params.fpass,'xtick',[0:10:1000],'xticklabel','', ...
        'ylim',ylim,'ytick',ylim(2),'yticklabel',ylim(2));
end

end


function plotPSTH(A,ClusterToPlot,TargetAx)
InTime = A.Data.TimeStamps>=A.TimeLim(1) & A.Data.TimeStamps<=A.TimeLim(2);
cBins = linspace(A.TimeLim(1),A.TimeLim(2),501);
BinWidth = mean(diff(cBins));
cBins = cBins(1:end-1)+diff(cBins);
cla(TargetAx)
for iPlot = 1:length(ClusterToPlot)
    iClust = A.ClusterNr == ClusterToPlot(iPlot);
    if ~any(iClust);continue;end
    if find(iClust)==A.currentCluster
        ColorIndex = 2;
    elseif ClusterToPlot(iPlot)==0 && get(findobj('tag','AlSortGui_SortModeAdd'),'value')==1
        ColorIndex = 2;
    else
        ColorIndex = 1;
    end
    TSIndex = InTime & A.SortBuffer(:,iClust,1);
    [HistN,HistBIN] = hist(A.Data.TimeStamps(TSIndex),cBins);
    h = PlotHistData(cBins',HistN'./(BinWidth/(10^6)),'LINEBAR', ...
        'parent',TargetAx, ...
        'color',A.ClusterColor(A.ClusterNr(iClust)+1,:,ColorIndex));
    
    [mn,mx] = GetDataRange(TargetAx);
    ylim = [0 max([max(mx(:,2,:))*1.0 1])];
    if isnan(ylim(2));ylim(2)=1;end
    set(TargetAx,'fontsize',10,'tickdir','out','ylim',ylim,'ytick',ylim(2),'yticklabel',ylim(2));
end
end
