function h = PlotHistData(X,Y,objtype,varargin)

% plots histogram data
% h = PlotHistData(X,Y,objtype,varargin)
% X ... centre of bins
% Y ... hist values
% objtype ... 'BAR' 'LINEBAR' 'LINE' 'PATCH'
% varargin ... object properties

h = [];
[nb,ng] = size(X);
switch upper(objtype)
     case 'BAR'
          h = bar(X,Y);
          if ~isempty(varargin)
               set(h,varargin{:});
          end
     case 'LINEBAR'
          [XDATA,YDATA] = makelinedata(X,Y);
          h = line(XDATA,YDATA,varargin{:});
     case 'LINE'
          h = line(X,Y,varargin{:});
     case 'PATCH'
          [XDATA,YDATA] = makelinedata(X,Y);
		  XDATA = [XDATA(1,:);XDATA;XDATA(end,:)];
		  YDATA = [zeros(1,ng);YDATA;zeros(1,ng)];
          h = patch('Xdata',XDATA,'ydata',YDATA,varargin{:});
      otherwise
          error(['Do not know plot type ' plottype ' ! Use BAR, LINE or PATCH.']);
end

function [XDATA,YDATA] = makelinedata(X,Y)
[nb,ng] = size(X);
xw = diff(X,[],1);
xlo = X-[xw(1,:);xw]./2;
xhi = X+[xw;xw(end,:)]./2;
XDATA = zeros(nb*2,ng);
XDATA(1:2:end-1,:) = xlo;
XDATA(2:2:end,:) = xhi;
YDATA = zeros(nb*2,ng);
YDATA(1:2:end-1,:) = Y;
YDATA(2:2:end,:) = Y;
